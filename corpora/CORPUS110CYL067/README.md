CORPUS110CYL067
---

Sample corpus of one syntactically parsed text from the [MASC](http://www.anc.org/data/masc) corpus to test TIGER Search queries in TXM (see [TIGER-XML import validation](https://groupes.renater.fr/wiki/txm-info/public/import_tiger#recette_alpha)). You need to install the [TIGERSearch ALPHA level](https://groupes.renater.fr/wiki/txm-users/public/extensions_alpha#tigersearch) extension to query with the TIGERSearch engine in TXM.

- **110CYL067.mrg**: the original syntactically parsed text from the [MASC](http://www.anc.org/data/masc) corpus in Penn Treebank format
- **CORPUS110CYL067-src.zip**: the source directory for the XML/w+CSV import module. The parsed text is in the TIGER-XML format
- **CORPUS110CYL067.txm**: the equivalent binary corpus to load into TXM

