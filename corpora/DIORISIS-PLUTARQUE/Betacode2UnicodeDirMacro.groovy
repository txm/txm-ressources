// Copyright © 2024 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License version 3 or any later version (https://www.gnu.org/licenses/gpl.html)
// @author sheiden

// STANDARD DECLARATIONS
package org.txm.macro.xml

import org.txm.utils.FileUtils;
import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.importer.StaxIdentityParser
import javax.xml.stream.XMLStreamException

@Field @Option(name="inputDirectory", usage="input directory (.xml files)", widget="Folder", required=true, def="")
def inputDirectory

@Field @Option(name="outputDirectory", usage="output directory", widget="Folder", required=true, def="")
def outputDirectory

@Field @Option(name="wordElement", usage="word element regex", widget="String", required=true, def="word")
def wordElement

@Field @Option(name="formAttribute", usage="word attribute name", widget="String", required=true, def="form")
def formAttribute

@Field @Option(name="backupAttribute", usage="word form backup attribute name", widget="String", required=true, def="b-form")
def backupAttribute

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

if (!inputDirectory.exists()) {
	println "** Betacode2UnicodeDir: no '"+inputDirectory.name+"' directory found. Aborting."
	return false
}

if (!inputDirectory.canRead()) {
	println "** Betacode2UnicodeDir: '"+inputDirectory.name+"' directory not readable. Aborting."
	return false
}

def f = []
inputDirectory.eachFileMatch(~/.*xml/) { f << it }

if (f.size() == 0) {
	println "** Betacode2UnicodeDir: no .xml file found. Aborting."
	return false
}

try {

f.sort{ it.name }.each { inputFile ->

	def outputFile = new File(outputDirectory, inputFile.name)

	res = gse.run(Betacode2UnicodeFileMacro, ["args":[

				"inputFile": inputFile,
				"outputFile": outputFile,
				"wordElement": wordElement,
				"formAttribute": formAttribute,
				"backupAttribute": backupAttribute,

				"selection":selection,
				"selections":selections,
				"corpusViewSelection":corpusViewSelection,
				"corpusViewSelections":corpusViewSelections,
				"monitor":monitor]])
				
			if (!res) println "** problem calling Betacode2UnicodeFileMacro."
}

} catch (Exception e) {
	println "** Betacode2UnicodeDir: unable to read input files. Aborting."
	println e.getLocalizedMessage()
	println e.printStackTrace()
	return false
}

return true

