<center>

# DIORISIS-PLUTARQUE Corpus

### Published by Serge Heiden
### Cactus, IHRIM, ENS de Lyon
### Under Licence [CC BY-NC-SA 3.0 FR](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/deed.fr)

</center>

### Description

Unicode version of the corpus of the 145 texts by Plutarch extracted from [The Diorisis Ancient
Greek Corpus](https://api.repository.cam.ac.uk/server/api/core/bitstreams/53203439-5568-4a98-922e-8e70350dd662/content):

- Vatri, A., & McGillivray, B. (2018).
The Diorisis Ancient Greek Corpus.
Research Data Journal for the
Humanities and Social Sciences. doi:
10.1163/24523666-00000000

The corpus is 1,111,190 words long, and the size of the texts is rather uneven:
![text sizes bar chart](diorisis-plutarque-texts-size.png "text sizes")

### Contents

- corpus **sources**: [Diorisis-Plutarque-src-unicode.zip](Diorisis-Plutarque-src-unicode.zip)
  - to import into TXM with the help of the XML-TEI XTZ+CSV import module
- TXM **binary** corpus: [DIORISIS-PLUTARQUE-2024-02-16.txm](DIORISIS-PLUTARQUE-2024-02-16.txm)
  - directly loadable into TXM

### Users

This corpus is currently used by the students of the “[Humanités classiques et humanités numériques](https://hclassiques.parisnanterre.fr)” (“Classical Studies and Digital Humanities”) Master programme at Paris-Nanterre University.

They publish the collective “[Classiques et numériques](https://classnum.hypotheses.org/)” research notebook containing some studies made with TXM.

### Sources

**Conversion from DIORISIS format to TEI format**

Sources include the [diorisis2tei.xsl](diorisis2tei.xsl) XSLT stylesheet which performs TEI conversion on the fly from the DIORISIS corpus format to an XML-TEI format compatible with TXM during import. It can therefore be used to build corpora of other authors from the DIORISIS corpus.

The word properties mapping from the DIORISIS XML format to XML-TEI format is the following:

- analysis ← lemma/analysis/@morph
- b-form ← Betacode version of form
- disambiguated ← lemma/@disambiguated
- foreign-id ← id
- lemma ← lemma/@entry
- lemma-id ← lemma/@id
- pos ← lemma/@POS
- treeTagger ← lemma/@TreeTagger
- word ← Unicode version of form

**Conversion from Betacode character encoding to Unicode**

Betacode to Unicode conversion table from The Perseids Project Tools repository: https://github.com/perseids-tools.

Conversion scripts:
- [Betacode2UnicodeParser](Betacode2UnicodeParser.groovy): conversion code
- [Betacode2UnicodeFile](Betacode2UnicodeFileMacro.groovy): TXM macro to convert one file (by applying Betacode2UnicodeParser)
- [Betacode2UnicodeDir](Betacode2UnicodeDirMacro.groovy): TXM macro to convert all the files in a folder (by applying Betacode2UnicodeFile)

Lemma were natively encoded in Unicode.

**Editions**

Airy editions by making sentences correspond to paragraphs (texts in the DIORISIS corpus have lost the structural encodings of their TEI sources):

![Airy editions](diorisis-plutarque-airy-editions.png "Airy editions")


**Concordance references**

Concordance references are composed of:
- text@ids
- text@date
- s@location

![Concordance references](diorisis-plutarque-concordance-references.png "Concordance references")


### Text Catalog and Metadata

- author
- title
- tlgAuthor
- tlgId
- lang
- date
- genre
- subgenre

```
author      title              tlgAuthor    tlgId  lang   date  genre     subgenre

Plutarch    Ad principem ineruditum    7    116    grc    95    Essays    Essay
Plutarch    Adversus Colotem    7    140    grc    95    Essays    Essay
Plutarch    Aemilius Paulus    7    19    grc    95    Narrative    Biography
Plutarch    Agesilaus    7    44    grc    95    Narrative    Biography
Plutarch    Agis    7    051a    grc    95    Narrative    Biography
Plutarch    Alcibiades    7    15    grc    95    Narrative    Biography
Plutarch    Alexander    7    47    grc    95    Narrative    Biography
Plutarch    Amatoriae narrationes    7    114    grc    95    Essays    Essay
Plutarch    Amatorius    7    113    grc    95    Essays    Essay
Plutarch    An Recte Dictum Sit Latenter Esse Vivendum    7    141    grc    95    Essays    Essay
Plutarch    An seni respublica gerenda sit    7    117    grc    95    Essays    Essay
Plutarch    An virtus doceri possit    7    93    grc    95    Essays    Essay
Plutarch    An vitiositas ad infelicitatem sufficia    7    99    grc    95    Essays    Essay
Plutarch    Animine an corporis affectiones sint peiores    7    100    grc    95    Essays    Essay
Plutarch    Antony    7    58    grc    95    Narrative    Biography
Plutarch    Apophthegmata Laconica    7    82    grc    95    Essays    Essay
Plutarch    Aquane an ignis sit utilior    7    128    grc    95    Essays    Essay
Plutarch    Aratus    7    63    grc    95    Narrative    Biography
Plutarch    Aristides    7    24    grc    95    Narrative    Biography
Plutarch    Artaxerxes    7    64    grc    95    Narrative    Biography
Plutarch    Bruta animalia ratione uti    7    130    grc    95    Essays    Essay
Plutarch    Brutus    7    61    grc    95    Narrative    Biography
Plutarch    Caesar    7    48    grc    95    Narrative    Biography
Plutarch    Caius Gracchus    7    052b    grc    95    Narrative    Biography
Plutarch    Caius Marcius Coriolanus    7    16    grc    95    Narrative    Biography
Plutarch    Caius Marius    7    31    grc    95    Narrative    Biography
Plutarch    Camillus    7    11    grc    95    Narrative    Biography
Plutarch    Cato the Younger    7    50    grc    95    Narrative    Biography
Plutarch    Cicero    7    55    grc    95    Narrative    Biography
Plutarch    Cimon    7    35    grc    95    Narrative    Biography
Plutarch    Cleomenes    7    051b    grc    95    Narrative    Biography
Plutarch    Comparationis Aristophanis et Menandri compendium    7    122    grc    95    Narrative    Biography
Plutarch    Comparison of Agesilaus and Pompey    7    46    grc    95    Narrative    Biography
Plutarch    Comparison of Agis and Cleomenes and the Gracchi    7    53    grc    95    Narrative    Biography
Plutarch    Comparison of Alcibiades and Coriolanus    7    17    grc    95    Narrative    Biography
Plutarch    Comparison of Aristides with Marcus Cato    7    26    grc    95    Narrative    Biography
Plutarch    Comparison of Demetrius and Antony    7    59    grc    95    Narrative    Biography
Plutarch    Comparison of Demosthenes with Cicero    7    56    grc    95    Narrative    Biography
Plutarch    Comparison of Dion and Brutus    7    62    grc    95    Narrative    Biography
Plutarch    Comparison of Lucullus and Cimon    7    37    grc    95    Narrative    Biography
Plutarch    Comparison of Lycurgus and Numa    7    6    grc    95    Narrative    Biography
Plutarch    Comparison of Lysander and Sulla    7    34    grc    95    Narrative    Biography
Plutarch    Comparison of Nicias and Crassus    7    40    grc    95    Narrative    Biography
Plutarch    Comparison of Pelopidas and Marcellus    7    23    grc    95    Narrative    Biography
Plutarch    Comparison of Pericles and Fabius Maximus    7    14    grc    95    Narrative    Biography
Plutarch    Comparison of Philopoemen and Titus    7    29    grc    95    Narrative    Biography
Plutarch    Comparison of Sertorius and Eumenes    7    43    grc    95    Narrative    Biography
Plutarch    Comparison of Solon and Publicola    7    9    grc    95    Narrative    Biography
Plutarch    Comparison of Theseus and Romulus    7    3    grc    95    Narrative    Biography
Plutarch    Comparison of Timoleon and Aemilius    7    20    grc    95    Narrative    Biography
Plutarch    Compendium Argumenti Stoicos absurdiora poetis dicere    7    137    grc    95    Essays    Essay
Plutarch    Compendium libri de animae procreatione in Timaeo    7    135    grc    95    Essays    Essay
Plutarch    Conjugalia Praecepta    7    78    grc    95    Essays    Essay
Plutarch    Consolatio ad Apollonium    7    76    grc    95    Oratory    Oratory
Plutarch    Consolatio ad uxorem    7    111    grc    95    Oratory    Oratory
Plutarch    Crassus    7    39    grc    95    Narrative    Biography
Plutarch    De Alexandri magni fortuna aut virtute    7    87    grc    95    Essays    Essay
Plutarch    De E apud Delphos    7    90    grc    95    Essays    Essay
Plutarch    De Herodoti malignitate    7    123    grc    95    Essays    Essay
Plutarch    De Iside et Osiride    7    89    grc    95    Essays    Essay
Plutarch    De Pythiae oraculis    7    91    grc    95    Essays    Essay
Plutarch    De Recta Ratione Audiendi    7    69    grc    95    Essays    Essay
Plutarch    De Se Ipsum Citra Invidiam Laudando    7    106    grc    95    Essays    Essay
Plutarch    De Stoicorum repugnantiis    7    136    grc    95    Essays    Essay
Plutarch    De amicorum multitudine    7    73    grc    95    Essays    Essay
Plutarch    De amore prolis    7    98    grc    95    Essays    Essay
Plutarch    De animae procreatione in Timaeo    7    134    grc    95    Essays    Essay
Plutarch    De capienda ex inimicis utilitate    7    72    grc    95    Essays    Essay
Plutarch    De cohibenda ira    7    95    grc    95    Essays    Essay
Plutarch    De communibus notitiis adversus Stoicos    7    138    grc    95    Essays    Essay
Plutarch    De cupiditate divitiarum    7    103    grc    95    Essays    Essay
Plutarch    De curiositate    7    102    grc    95    Essays    Essay
Plutarch    De defectu oraculorum    7    92    grc    95    Essays    Essay
Plutarch    De esu carnium I    7    131    grc    95    Essays    Essay
Plutarch    De esu carnium II    7    132    grc    95    Essays    Essay
Plutarch    De exilio    7    110    grc    95    Essays    Essay
Plutarch    De faciae quae in orbe lunae apparet    7    126    grc    95    Essays    Essay
Plutarch    De fato    7    108    grc    95    Essays    Essay
Plutarch    De fortuna    7    74    grc    95    Essays    Essay
Plutarch    De fortuna Romanorum    7    86    grc    95    Essays    Essay
Plutarch    De fraterno amore    7    97    grc    95    Essays    Essay
Plutarch    De garrulitate    7    101    grc    95    Essays    Essay
Plutarch    De genio Socratis    7    109    grc    95    Essays    Essay
Plutarch    De gloria Atheniensium    7    88    grc    95    Essays    Essay
Plutarch    De invidia et odio    7    105    grc    95    Essays    Essay
Plutarch    De liberis educandis    7    67    grc    95    Essays    Essay
Plutarch    De primo frigido    7    127    grc    95    Essays    Essay
Plutarch    De sera numinis vindicta    7    107    grc    95    Essays    Essay
Plutarch    De sollertia animalium    7    129    grc    95    Essays    Essay
Plutarch    De superstitione    7    80    grc    95    Essays    Essay
Plutarch    De tranquilitate animi    7    96    grc    95    Essays    Essay
Plutarch    De tuenda sanitate praecepta    7    77    grc    95    Essays    Essay
Plutarch    De unius in republica dominatione    7    119    grc    95    Essays    Essay
Plutarch    De virtute et vitio    7    75    grc    95    Essays    Essay
Plutarch    De virtute morali    7    94    grc    95    Essays    Essay
Plutarch    De vitando aere alieno    7    120    grc    95    Essays    Essay
Plutarch    De vitioso pudore    7    104    grc    95    Essays    Essay
Plutarch    Demetrius    7    57    grc    95    Narrative    Biography
Plutarch    Demosthenes    7    54    grc    95    Narrative    Biography
Plutarch    Dion    7    60    grc    95    Narrative    Biography
Plutarch    Eumenes    7    41    grc    95    Narrative    Biography
Plutarch    Fabius Maximus    7    13    grc    95    Narrative    Biography
Plutarch    Galba    7    65    grc    95    Narrative    Biography
Plutarch    Instituta Laconica    7    082a    grc    95    Essays    Essay
Plutarch    Lacaenarum Apophthegmata    7    082b    grc    95    Essays    Essay
Plutarch    Lucullus    7    36    grc    95    Narrative    Biography
Plutarch    Lycurgus    7    4    grc    95    Narrative    Biography
Plutarch    Lysander    7    32    grc    95    Narrative    Biography
Plutarch    Marcellus    7    22    grc    95    Narrative    Biography
Plutarch    Marcus Cato    7    25    grc    95    Narrative    Biography
Plutarch    Maxime cum principbus philosopho esse diserendum    7    115    grc    95    Essays    Essay
Plutarch    Mulierum virtutes    7    83    grc    95    Essays    Essay
Plutarch    Nicias    7    38    grc    95    Narrative    Biography
Plutarch    Non posse suaviter vivi secundum Epicurum    7    139    grc    95    Essays    Essay
Plutarch    Numa    7    5    grc    95    Narrative    Biography
Plutarch    Otho    7    66    grc    95    Narrative    Biography
Plutarch    Parallela minora    7    85    grc    95    Narrative    Biography
Plutarch    Pelopidas    7    21    grc    95    Narrative    Biography
Plutarch    Pericles    7    12    grc    95    Narrative    Biography
Plutarch    Philopoemen    7    27    grc    95    Narrative    Biography
Plutarch    Phocion    7    49    grc    95    Narrative    Biography
Plutarch    Platonicae quaestiones    7    133    grc    95    Essays    Essay
Plutarch    Pompey    7    45    grc    95    Narrative    Biography
Plutarch    Praecepta gerendae reipublicae    7    118    grc    95    Essays    Essay
Plutarch    Publicola    7    8    grc    95    Narrative    Biography
Plutarch    Pyrrhus    7    30    grc    95    Narrative    Biography
Plutarch    Quaestiones Convivales    7    112    grc    95    Essays    Essay
Plutarch    Quaestiones Graecae    7    084b    grc    95    Essays    Essay
Plutarch    Quaestiones Naturales    7    125    grc    95    Essays    Essay
Plutarch    Quaestiones Romanae    7    084a    grc    95    Essays    Essay
Plutarch    Quomodo adolescens poetas audire debeat    7    68    grc    95    Essays    Essay
Plutarch    Quomodo adulator ab amico internoscatur    7    70    grc    95    Essays    Essay
Plutarch    Quomodo quis suos in virtute sentiat profectus    7    71    grc    95    Essays    Essay
Plutarch    Regum et imperatorum apophthegmata    7    81    grc    95    Essays    Essay
Plutarch    Romulus    7    2    grc    95    Narrative    Biography
Plutarch    Septem sapientium convivium    7    79    grc    95    Essays    Essay
Plutarch    Sertorius    7    42    grc    95    Narrative    Biography
Plutarch    Solon    7    7    grc    95    Narrative    Biography
Plutarch    Sulla    7    33    grc    95    Narrative    Biography
Plutarch    Themistocles    7    10    grc    95    Narrative    Biography
Plutarch    Theseus    7    1    grc    95    Narrative    Biography
Plutarch    Tiberius Gracchus    7    052a    grc    95    Narrative    Biography
Plutarch    Timoleon    7    18    grc    95    Narrative    Biography
Plutarch    Titus Flamininus    7    28    grc    95    Narrative    Biography
Plutarch    Vitae decem oratorum    7    121    grc    95    Narrative    Biography
```