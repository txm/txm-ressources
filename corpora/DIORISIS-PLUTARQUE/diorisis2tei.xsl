<?xml version="1.0"?>
<xsl:stylesheet
  xmlns:xd="http://www.pnp-software.com/XSLTdoc"
  xmlns:edate="http://exslt.org/dates-and-times"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:tei="http://www.tei-c.org/ns/1.0"
  exclude-result-prefixes="tei edate xd" version="2.0">
 
  <xd:doc type="stylesheet">
    <xd:copyright>2021, ENS de Lyon / IHRIM (Cactus)</xd:copyright>
    <xd:author>Serge Heiden slh@ens-lyon.fr</xd:author>
    <xd:detail>LICENSE
			This stylesheet is free software; you can redistribute it and/or
			modify it under the terms of the GNU Lesser General Public
			License as published by the Free Software Foundation; either
			version 3 of the License, or (at your option) any later version.
			
			This stylesheet is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
			Lesser General Public License for more details.
			
			You should have received a copy of GNU Lesser Public License with
			this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
		</xd:detail>
  </xd:doc>
  

  <xsl:output method="xml" encoding="utf-8" omit-xml-declaration="yes"/>

  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="*|@*|processing-instruction()|comment()|text()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="TEI.2">
    <TEI xmlns="http://www.tei-c.org/ns/1.0">
      <xsl:apply-templates
        select="*|@*|processing-instruction()|comment()|text()" />
    </TEI>
  </xsl:template>

    <xsl:template match="sentence">
    <xsl:element name="p" namespace="{namespace-uri()}">
      <xsl:element name="s" namespace="{namespace-uri()}">
        <xsl:copy-of select="@*"/>
        <xsl:apply-templates/>
      </xsl:element>
    </xsl:element>
  </xsl:template>

<!--

        <word form="potamw=n" id="2">
          <lemma id="85953" entry="ποταμός" POS="noun" TreeTagger="false" disambiguated="n/a">
            <analysis morph="masc gen pl"/>
          </lemma>
        </word>
        <punct mark=","/>
-->

  <xsl:template match="word">
    <xsl:element name="w" namespace="{namespace-uri()}">

      <xsl:attribute name="lemma">
        <xsl:value-of select="lemma/@entry" />
      </xsl:attribute>
      
      <xsl:attribute name="lemma-id">
        <xsl:value-of select="lemma/@id" />
      </xsl:attribute>
      
      <xsl:attribute name="pos">
        <xsl:value-of select="lemma/@POS" />
      </xsl:attribute>
      
      <xsl:attribute name="TreeTagger">
        <xsl:value-of select="lemma/@TreeTagger" />
      </xsl:attribute>
      
      <xsl:attribute name="disambiguated">
        <xsl:value-of select="lemma/@disambiguated" />
      </xsl:attribute>
      
      <xsl:attribute name="analysis">|<xsl:for-each select="lemma/analysis/@morph">
          <xsl:if test="position() &gt; 1">|</xsl:if>
          <xsl:value-of select="."/>
        </xsl:for-each>|</xsl:attribute>
  
		  <xsl:if test="@b-form">
        <xsl:attribute name="b-form">
          <xsl:value-of select="@b-form" />
        </xsl:attribute>
      </xsl:if>    
      
      <xsl:copy-of select="@id"/>
      
      <xsl:value-of select="@form" />
      
    </xsl:element>    
  </xsl:template>

  <xsl:template match="punct">
    <xsl:element name="w" namespace="{namespace-uri()}">
      <xsl:attribute name="lemma"><xsl:value-of select="@mark" /></xsl:attribute>
      <xsl:attribute name="pos">punct</xsl:attribute>
      <xsl:value-of select="@mark" />
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>

