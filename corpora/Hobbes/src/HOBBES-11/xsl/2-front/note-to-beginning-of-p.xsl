<?xml version="1.0"?>
<xsl:stylesheet
  xmlns:xd="http://www.pnp-software.com/XSLTdoc"
  xmlns:edate="http://exslt.org/dates-and-times"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:tei="http://www.tei-c.org/ns/1.0"
  exclude-result-prefixes="tei edate xd" version="2.0">
 
  <xd:doc type="stylesheet">
    <xd:copyright>2022, ENS de Lyon / IHRIM (Cactus)</xd:copyright>
    <xd:author>Serge Heiden, slh@ens-lyon.fr</xd:author>
    <xd:author>Alexei Lavrentiev, CNRS</xd:author>
  </xd:doc>
  

  <xsl:output method="xml"/>

<xsl:template match="node()|@*">
  <xsl:copy>
    <xsl:apply-templates select="node()|@*" />
  </xsl:copy>
 </xsl:template>  

  <xsl:template match="tei:text//tei:p">
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <xsl:copy-of select="descendant::tei:note"/>
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="tei:text//tei:note"/>
   
</xsl:stylesheet>

