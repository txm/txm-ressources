
P1S8 4 avril 2014 transcription et enregistrement d'un cours de physique en Lycée
===

Ce corpus illustre les 2 niveaux d'importation possibles de transcriptions d'enregistrements dans TXM :

* des transcriptions au format **traitement de texte** (extensions `.odt`, `.doc`, `.rtf`), éditables avec LibreOffice Writer, MS Word
* des transcriptions au format **Transcriber** (extension `.trs`), éditables avec le logiciel Transcriber[^1]

La transcription est conforme aux conventions de transcription de la chaine d'importation de transcriptions au format traitement de texte dans le logiciel TXM décrite dans le [Tutoriel d'import de transcriptions d'enregistrements dans TXM](https://groupes.renater.fr/wiki/txm-users/public/tutoriel_import_transcriptions). Elle peut donc servir d'exemple pour l'application de ce tutoriel.

La vidéo et le son correspondant à cette transcription sont jouables depuis TXM en installant l'extension "Media Player".

Contenu
---

* Le dossier 'p1s8-course-transcription' contient :

  A) différentes versions de la même transcription source « P1S8 4 avril 2014 »

   * P1S8 30 avril 2014.rtf : fichier original à partir duquel tous les autres ont été produits
   * P1S8 30 avril 2014.odt : fichier éditable avec LibreOffice Writer
   * P1S8 30 avril 2014.doc : fichier éditable avec MS Word
   * P1S8 30 avril 2014.pdf : fichier imprimable
   * P1S8 30 avril 2014.trs : fichier source au format Transcriber produit par la macro 'TextTranscription2TRS', à importer dans TXM avec le module 'XML Transcriber+CSV'
   * P1S8 30 avril 2014.xml : fichier source interne à TXM, au format étendu 'XML TEI TXM'
   * trans-14.dtd : un fichier annexe nécessaire à l'importation du fichier .trs

  B) L'enregistrement du cours à partir duquel a été établie la transcription :

   * P1S8 30 avril 2014.mp4 : vidéo anonymisée, seule la partie située entre 0h30m26s et 0h31m39s est visualisable
   * P1S8 30 avril 2014.mp3 : audio anonymisé, seule la partie située entre 0h30m26s et 0h31m39s est audible

  C) le fichier journal 'TextTranscription2TRSMacroP1S8Log.txt' qui est un exemple détaillé des messages d'exécution de la macro TextTranscription2TRS appliquée à un répertoire contenant le fichier 'P1S8 30 avril 2014.odt'.\
Remarque : la macro peut transformer les transcriptions aux formats : rtf, odt et doc.

* Le dossier 'bin' contient le corpus binaire TXM correspondant à cette unique transcription lemmatisée par TreeTagger :

  * P1S8-2023-06-23.txm : construit à partir du dossier source 'P1S8', à charger directement dans TXM (pas besoin de faire un import)

* Le dossier 'src' contient des exemples de dossiers source à importer directement dans TXM avec le module 'Transcriber + CSV' :

  * P1S8 : les sources avec la vidéo limitée au moment de la transcription
  * P1S8full : les sources avec la vidéo complète

Licences
---

Transcriptions et enregistrements : Copyright © 2013-2014 Andrée Tiberghien, Marie-Pierre Chopin, Laurent Lima, Laurent Talbot, Abdelkarim Zaid

Documents annexes : Copyright © 2013-2014 ENS de Lyon

Les transcriptions, les enregistrements et les documents annexes sont diffusés sous licence Creative Commons BY-NC-SA : [http://creativecommons.org/licenses/by-nc-sa/3.0/fr](http://creativecommons.org/licenses/by-nc-sa/3.0/fr)

Références
---

La vidéo à l'origine de cette transcription et les études qui ont été menées
à partir d'elle sont décrites dans un numéro spécial de la revue « éducation & didactique » :

Tiberghien Andrée et al., « Partager un corpus vidéo dans la recherche en éducation :  
analyses et regards pluriels dans le cadre du projet ViSA », Education & didactique 3/ 2012 (vol.6), p. 9-17  
URL : [www.cairn.info/revue-education-et-didactique-2012-3-page-9.htm](http://www.cairn.info/revue-education-et-didactique-2012-3-page-9.htm).  
DOI : 10.4000/educationdidactique.1686

Contributions
---
Les différentes versions de transcriptions (la même transcription dans divers formats) ont été produites par Serge Heiden à partir des sources RTF d'Andrée Tiberghien pour le tutoriel de transformation de transcriptions d'enregistrements au format ODT
vers le format TRS pour import dans TXM (voir ci-dessous) pour le [projet Textométrie](https://www.textometrie.org).

Les différents fichiers média ont été préparés par Justine Lascar à partir des médias originaux d'Andrée Tiberghien.

Notes
---

[^1]: on peut également éditer une transcription avec tout logiciel capable d'exporter dans le format .trs ou d'exporter dans un format convertible par la passerelle en ligne [TeiConvert](https://ct3.ortolang.fr/teiconvert/index-fr.html) (qui peut convertir vers le format .trs)