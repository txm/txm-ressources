Sample Corpora
===

Go to a corpus directory, download the .txm binary file, and then call the 'File > Load' command in TXM to load it.

For some corpora the sources are also provided, so you can also import from the sources and tune the corpus.

## Written texts

**French**

- **discours**: corpus of various French presidents’ speeches, published by Damon Mayaffre.
- **fleurs-du-mal**: Les Fleurs du mal (The Flowers of Evil) by Charles Baudelaire, edition of Jean-Marie Viprey.
- **mpt**: corpus of French National Assembly debates on the "Mariage pour tous" law of 2013 from the [mariagepourtousInXML project](https://github.com/nlegrand/mariagepourtousInXML).
- **quete-du-graal-tei**: Queste del Saint Graal (Quest for the Holy Grail), edition of Christiane-Marchello Nizia and Alexei Lavrentiev, based on 'Lyon, Palais des Arts 77 (ms. K) (fol. 160a-224d)' and 'Paris, BNF n. acq. fr. 1119 (ms. Z)' ca. 1225 or 1230 Old French manuscripts.
- **tdm80j**: Le tour du monde en quatre-vingts jours (Around the World in Eighty Days), Jules Verne, 1873, edition of J. Hetzel et Cie. Synoptic edition with Wikisource facsimile images.
- **txm-odt-manual**: TXM User's manual as a TXM corpus.
- **VOEUX**: corpus of 63 New Year’s Day speeches by French presidents (1959-2021), published by Jean-Marc Leblanc & Serge Heiden.

**English**

- **brown**: corpus of 500 texts written in American English in 1961, published by W. N. Francis et H. Kucera (this version based on the XML TEI version of [NLTK project](https://nltk.googlecode.com/svn/trunk/nltk_data/index.xml)).
- **leviathan**: Leviathan by Thomas Hobbes, 1588-1679. XML-TEI P5 text sample from the EEBO-TCP Phase 1 project.

**German**

- **voeux-rfa**: corpus of the Christmas and the New Year's addresses delivered by the Presidents and the Chancellors of the Federal Republic of Germany since 1987, contributed by Sascha Diwersy, Universität zu Köln.

## Record transcriptions (synchronized)

- **p1s8-course-transcription**: French Speech transcription and recording of a high school course of physics, Tiberghien Andrée et al., [Education & didactique 3/ 2012 (vol.6)](https://www.cairn.info/revue-education-et-didactique-2012-3-page-9.htm). To practice video replay from concordances (needs Media Player extension).

## Parallel corpora (multilingual)

- **uno-tmx-sample**: sample of United Nations General Assembly Resolutions: A Six-Language Parallel Corpus (Arabic, Chinese, English, French, Russian and Spanish), https://www.uncorpora.org [Alexandre Rafalovitch, Robert Dale. 2009. United Nations General Assembly Resolutions: A Six-Language Parallel Corpus. In Proceedings of the MT Summit XII, pages 292-299, Ottawa, Canada, August]. To import with the XML-TMX import module.

## Annotated corpora

- **CORPUS110CYL067**: a single syntactically parsed text from the [MASC](https://www.anc.org/data/masc) corpus. To practice TIGER Search queries (see [TIGER-XML import validation](https://groupes.renater.fr/wiki/txm-info/public/import_tiger#recette_alpha), needs TIGER Search extension).

Some corpora are also available from the [TXM portals](https://groupes.renater.fr/wiki/txm-users/public/references_portails).

