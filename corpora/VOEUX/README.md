<center>

# Corpus VOEUX<br/>-<br/>63 Discours de voeux des présidents français (1959-2021)

### publié par Jean-Marc Leblanc & Serge Heiden
### sous licence [CC BY-NC-SA 3.0 FR](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/deed.fr)

</center>

Entrepôt de référence du corpus VOEUX pour TXM.

Contenu des dossiers :
- src : sources du corpus, en différentes versions et formats
- bin : corpus binaires, téléchargeables et chargeables directement dans TXM

<center>

# 63 New Year’s Day speeches by French presidents (1959-2021)

### published by Jean-Marc Leblanc & Serge Heiden
### under licence [CC BY-NC-SA 3.0 FR](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/deed.fr)

</center>

Reference repository of the VOEUX corpus for TXM.

Contents of folders:
- src: corpus sources, in different versions and formats
- bin: binary corpora, downloadable and loadable directly into TXM