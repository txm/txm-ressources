© 2010 Jean-Marie Viprey
---

The FLEURS (Fleurs du Mal) corpus by Jean-Marie Viprey is licensed under a Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/3.0/ or send a letter to Creative Commons, 171 Second Street, Suite 300, San Francisco, California, 94105, USA.

This version of the corpus that is compatible with the TXM platform is provided by the Textométrie research project (http://textometrie.ens-lyon.fr).
