Erreurs Populaires, extrait de 3 pages du chapitre 2 du livre 1
===

Fichier .xml exemple extrait de trois pages du chapitre 2 du premier livre de l'ouvrage Erreurs populaires de 
Laurent Joubert.

Contenu
---

Fichier `joubert1579_1-02-simple.xml` encodé en XML-TEI.


Contributeurs
---

* Laurent Joubert : Auteur du texte
* Alexei Lavrentiev : Transcription et Encodage TEI

Licence
---

Diffusé sous licence Creative Commons [BY](https://creativecommons.org/licenses/by/4.0/deed.fr).

Références
---

Voir l'[édition numérique](https://txm-ihrim.huma-num.fr/txm/?command=Documentation&path=/JOUBERT) d'origine.

