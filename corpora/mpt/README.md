MPT
===

Corpus of French National Assembly debates on the "Mariage pour tous" law of 2013 from the [mariagepourtousInXML](https://github.com/nlegrand/mariagepourtousInXML) GitHub project adapted for TXM 0.7.8 XTZ+CSV import module.

* mpt-src.zip: source files
* mpt.txm: binary corpus to load into TXM


*COPYRIGHT*
There is no copyright for french public or official documents as said
here: <http://www.assemblee-nationale.fr/faq.asp>

*AUTHORS*
- compilation of sources and XML encoding: Nicolas Legrand (see <https://github.com/nlegrand/mariagepourtousInXML>)
- source bundle for TXM 0.7.8 XTZ+CSV import: Serge Heiden (see <http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=35>)

*LICENSE*
Everything under CC by: <http://creativecommons.org/licenses/by/3.0/>

