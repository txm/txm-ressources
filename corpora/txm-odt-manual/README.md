REFMAN corpus
======

TXM 0.7.8 manual provided as a TXM corpus for perusal:
*Manuel de TXM, Version 0.7.8, Août 2017, Serge Heiden & al*.

Other versions of the manual:

* HTML to read online: [https://txm.gitpages.huma-num.fr/textometrie/Documentation/#manuel-de-txm-07)
* Original LibreOffice ODT source : [https://sourceforge.net/p/txm/code/HEAD/tree/trunk/doc/user%20manuals/Manuel%20de%20TXM%200.7%20FR.odt](https://sourceforge.net/p/txm/code/HEAD/tree/trunk/doc/user%20manuals/Manuel%20de%20TXM%200.7%20FR.odt)

The ODT source file as been imported into TXM by the ODT/DOC/RTF+CSV import module.

Words have been annotated by TreeTagger.

Edition includes the original images of the manual.

Copyright ⓒ 2008-2018 ENS de Lyon

The REFMAN corpus is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.

Provided by the TXM team

