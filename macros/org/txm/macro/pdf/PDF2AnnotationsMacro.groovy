package org.txm.macro.pdf
// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.apache.pdfbox.Loader
import org.apache.pdfbox.text.PDFTextStripperByArea
import org.apache.pdfbox.cos.COSName
import java.awt.geom.Rectangle2D
import java.awt.geom.Rectangle2D.Float
import org.apache.pdfbox.cos.COSArray
import org.apache.pdfbox.cos.COSFloat
import org.apache.pdfbox.pdmodel.common.PDRectangle

// BEGINNING OF PARAMETERS

@Field @Option(name="input_file", usage=".pdf input file", widget="File", required=false, def="")
def input_file

@Field @Option(name="input_dir", usage="The directory containing the .pdf files to read", widget="Folder", required=false, def="")
def input_dir


// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

ArrayList<String> highlightedTexts = new ArrayList<>()

if (input_dir != null &&  input_dir.exists()) {
	nFiles = 0
	input_dir.eachFileMatch(~/.*.pdf/) { f ->
		if (f.getAbsolutePath().toUpperCase().endsWith(".PDF")) {
	 		processFile(f)
	 		nFiles++
		}
	}
	println "Processed "+nFiles+" files."
} else {
	if (input_file.getAbsolutePath().toUpperCase().endsWith(".PDF")) {
	 processFile(input_file)
	}
}

def processFile(input_file) {

		name = input_file.getName()
		dir = input_file.getParentFile()
		idx = name.lastIndexOf(".")
		if (idx > 0) name = name.substring(0, idx)
		outputFile = new File(input_file.getParentFile(), name + "-annotations.tsv")
		
		println "Processing "+name+"..."
		
		try {
			outputFile.withWriter("UTF-8") { writer ->
			
			doc = Loader.loadPDF(input_file)
			
			writer.println "page\ttype\thighlight\tcomment"
			
			for (npage = 0; npage < doc.getNumberOfPages(); ++npage) {

				page = doc.getPage(npage)
				al = page.getAnnotations()
				stripper = new PDFTextStripperByArea()
				stripper.setSortByPosition(true)
				
				al.each {
					if (it.getSubtype().equals("Highlight")) {
						s = (npage+1)+"\t"+it.getSubtype()
						writer.print s
						
						// adapted from https://www.anycodings.com/1questions/5047580/java-apache-pdfbox-extract-highlighted-text
				
						quadsArray = it.getCOSObject().getDictionaryObject(COSName.getPDFName("QuadPoints"))
            					str = null

            					for(int j=1, k=0; j<=(quadsArray.size()/8); j++) {

                					ULX = quadsArray.get(0+k)
                					ULY = quadsArray.get(1+k)
                					URX = quadsArray.get(2+k)
                					URY = quadsArray.get(3+k)
                					LLX = quadsArray.get(4+k)
                					LLY = quadsArray.get(5+k)
                					LRX = quadsArray.get(6+k)
                					LRY = quadsArray.get(7+k)

                					k+=8

                					ulx = ULX.floatValue() - 1			// upper left x.
                					uly = ULY.floatValue()				// upper left y.
                					width = URX.floatValue() - LLX.floatValue()	// calculated by upperRightX - lowerLeftX.
                					height = URY.floatValue() - LLY.floatValue()	// calculated by upperRightY - lowerLeftY.

                					PDRectangle pageSize = page.getMediaBox()
                					uly = pageSize.getHeight() - uly

                					rectangle_2 = new Rectangle2D.Double(ulx, uly, width, height)
                					stripper.addRegion("highlightedRegion", rectangle_2)
                					stripper.extractRegions(page)
                					highlightedText = stripper.getTextForRegion("highlightedRegion").trim()

                					if(j > 1) {
                					    str = str+" "+highlightedText
                					} else {
                					    str = highlightedText
                					}
            					}
            					if (str.length() > 0) {
            						writer.print '\t"'+str.replaceAll('"', '""')+'"'
            					} else {
            						writer.print '\t""'
            					}
            					if (it.getContents() != null) {
							writer.println '\t"'+it.getContents().replaceAll('"', '""')+'"'
						} else {
            						writer.println '\t""'
            					}
					} else {
						if (it.getContents() != null) {
							s = (npage+1)+"\t"+it.getSubtype()
							writer.print s
							s = '\t""\t"'+it.getContents().replaceAll('"', '""')+'"'
							writer.println s
            					}
					}
			    	}
			}
			doc.close()
			writer.close()
			} // writer
		} catch (Exception e) {
			e.printStackTrace()
		}
}

