package org.txm.macro.pdf
// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.apache.pdfbox.Loader
import org.apache.pdfbox.rendering.PDFRenderer
import org.apache.pdfbox.tools.imageio.ImageIOUtil
import org.apache.pdfbox.rendering.ImageType

// BEGINNING OF PARAMETERS

@Field @Option(name="input_file", usage=".pdf input file", widget="File", required=false, def="")
def input_file

@Field @Option(name="input_dir", usage="The directory containing the .pdf files to read", widget="Folder", required=false, def="")
def input_dir

@Field @Option(name="image_density", usage="image density (e.g. 300 for printer or 75 for screen)", widget="Integer", required=true, def="300")
def image_density

@Field @Option(name="image_format", usage="image format (jpg, gif, tiff or png)", widget="StringArray", required=true, metaVar="gif	jpg	png	tiff", def="png")
def image_format

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

if (input_dir != null &&  input_dir.exists()) {
	nFiles = 0
	input_dir.eachFileMatch(~/.*.pdf/) { f ->
		if (f.getAbsolutePath().toUpperCase().endsWith(".PDF")) {
	 		processFile(f)
	 		nFiles++
		}
	}
	println "Processed "+nFiles+" files."
} else {
	if (input_file.getAbsolutePath().toUpperCase().endsWith(".PDF")) {
	 processFile(input_file)
	}
}

def processFile(input_file) {

	name = input_file.getName()
	dir = input_file.getParentFile()
	idx = name.lastIndexOf(".")
	if (idx > 0) name = name.substring(0, idx)

	pdfFile = input_file.getAbsolutePath()
	if (pdfFile.toUpperCase().endsWith(".PDF")) {
	
		println "Processing "+name+"..."
		
		try {
			doc = Loader.loadPDF(input_file)
			pdfRenderer = new PDFRenderer(doc)
			nPages = doc.getNumberOfPages()
			widthNPages = Math.log10(nPages)+1 as int
			for (page = 0; page < nPages; ++page) {
    				bim = pdfRenderer.renderImageWithDPI(page, image_density, ImageType.RGB)
    				fileName = sprintf("%s-%0"+widthNPages+"d.%s", name, page+1, image_format)
    				fos = new FileOutputStream(new File(dir, fileName))
						ImageIOUtil.writeImage(bim, image_format, fos, image_density)
						print "."
			}
			println " Done."
			fos.close()
			doc.close()
		} catch (Exception e) {
			e.printStackTrace()
		}
	}
}


