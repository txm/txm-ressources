#!/bin/bash

if [ $# == 0 ]
then
echo "Usage: conllDir2tiger.sh input_directory_path"
exit 0
fi

if [ ! -d $1 ]
then
echo "** impossible to access directory $1. Aborting"
exit 1
fi

cd $1

rm -rf tiger-xml
echo "mkdir tiger-xml"
mkdir tiger-xml

echo "Converting..."

> tiger-xml/subcorpus-list

for f in *.conllu
do
echo $f...
perl conll2tiger-profiterole-ud.pl -s 10000 -o tiger-xml $f
grep '<subcorpus ' tiger-xml/main.xml >> tiger-xml/subcorpus-list
done

echo "subcorpus-list:"
cat tiger-xml/subcorpus-list

sed -e "/<subcorpus /{rtiger-xml/subcorpus-list" -e "d}" < tiger-xml/main.xml > tiger-xml/main-full.xml

rm tiger-xml/subcorpus-list
rm tiger-xml/conversion.log
mv tiger-xml/main-full.xml tiger-xml/main.xml

echo "Done."

