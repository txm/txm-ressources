<!-- The Identity Transformation -->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  
  <!-- This stylesheet comments out the sentences containing more than 100 terminal nodes in Tiger XML files.
    To change the theshold, puse the maxLength parameter.
  
  Written by A. Lavrentiev, CNRS, UMR IHRIM 2021-07-16
  Licence: GNU GPL v.3
  -->
  
  <!-- Whenever you match any node or any attribute -->
  <xsl:template match="node()|@*">
    <!-- Copy the current node -->
    <xsl:copy>
      <!-- Including any attributes it has and any child nodes -->
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:param name="maxLength" as="xs:integer">100</xsl:param>
  
  <xsl:template match="s">
    <xsl:choose>
      <xsl:when test="count(graph/terminals/t) gt $maxLength">
        <xsl:comment>
          Sentence too long (<xsl:value-of select="count(graph/terminals/t)"/> tokens) :
          <xsl:for-each select="graph/terminals/t"><xsl:value-of select="@word"/><xsl:text> </xsl:text></xsl:for-each>
        </xsl:comment>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
</xsl:stylesheet>
