<!-- The Identity Transformation -->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="#all">
  
  <!-- This stylesheet sorts the BFM texts by date in the main.xml file of TIGER Search indexes.
  To use for other texts, replace the text-order variable. There must be a text element for every text file 
  where the @id is the file name and @order is the sorting key.
  
  Written by A. Lavrentiev, CNRS, UMR IHRIM, 2021-07-16
  GNU GPL v.3
  -->
  
  
  <!-- Whenever you match any node or any attribute -->
  <xsl:template match="node()|@*">
    <!-- Copy the current node -->
    <xsl:copy>
      <!-- Including any attributes it has and any child nodes -->
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:output indent="yes"/>
  
  <xsl:template match="corpus">
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <xsl:apply-templates select="head"/>
      <body>
        <xsl:for-each select="$text-order/text">
          <xsl:sort select="@order"/>
          <xsl:variable name="text-id"><xsl:value-of select="@id"/></xsl:variable>
          <xsl:for-each select="$text-list/text">
            <xsl:if test="@id = $text-id">
              <subcorpus name='{@id}-{@n}' external='file:{@id}-{@n}.xml'/>
            </xsl:if>
          </xsl:for-each>
        </xsl:for-each>
      </body>
    </xsl:copy>
  </xsl:template>
  
  
  <xsl:variable name="text-list">
    <xsl:for-each select="//subcorpus">
      <text id="{replace(@name,'-\d+$','')}" n="{replace(@name,'^.+-(\d+)$','$1')}"></text>
    </xsl:for-each>
  </xsl:variable>
  
  <xsl:variable name="text-order">
    <text id="strasbBfm" order="0842strasbBfm"/>
    <text id="eulaliBfm" order="0883eulaliBfm"/>
    <text id="jonas" order="0945jonas"/>
    <text id="passion" order="1000passion"/>
    <text id="slethgier" order="1000slethgier"/>
    <text id="AlexisS2_Prologue" order="1050aAlexisS2_Prologue"/>
    <text id="AlexisRaM" order="1050bAlexisRaM"/>
    <text id="roland" order="1100roland"/>
    <text id="leiswillelm" order="1112leiswillelm"/>
    <text id="comput" order="1116comput"/>
    <text id="EpreuveJudic" order="1117EpreuveJudic"/>
    <text id="Lapidal" order="1117Lapidal"/>
    <text id="oxfps" order="1125oxfps"/>
    <text id="bestiaire" order="1128bestiaire"/>
    <text id="gormont" order="1130gormont"/>
    <text id="Juise" order="1137Juise"/>
    <text id="DescrEngl" order="1140DescrEngl"/>
    <text id="guill1" order="1140guill1"/>
    <text id="Lapidfp" order="1150Lapidfp"/>
    <text id="PsArundP" order="1150PsArundP"/>
    <text id="PsOrne" order="1150PsOrne"/>
    <text id="thebes1" order="1150thebes1"/>
    <text id="thebes2" order="1150thebes2"/>
    <text id="brut2" order="1155brut2"/>
    <text id="ChronSMichelBo" order="1155ChronSMichelBo"/>
    <text id="eneas1" order="1155eneas1"/>
    <text id="eneas2" order="1155eneas2"/>
    <text id="floire_jl" order="1155floire_jl"/>
    <text id="cambps" order="1157cambps"/>
    <text id="pyrame" order="1157pyrame"/>
    <text id="mf" order="1160mf"/>
    <text id="CommPsia1a" order="1164CommPsia1a"/>
    <text id="ProvSerlo" order="1165ProvSerlo"/>
    <text id="philomena" order="1167philomena"/>
    <text id="ErecKu" order="1170ErecKu"/>
    <text id="narcisse_lai" order="1170narcisse_lai"/>
    <text id="rou1" order="1170rou1"/>
    <text id="rou2" order="1170rou2"/>
    <text id="becket" order="1173becket"/>
    <text id="BenDuc1b" order="1174BenDuc1b"/>
    <text id="Fantosme" order="1174Fantosme"/>
    <text id="thomas" order="1174thomas"/>
    <text id="MirNDOrl" order="1175MirNDOrl"/>
    <text id="RecMedJute" order="1175RecMedJute"/>
    <text id="CligesKu" order="1176CligesKu"/>
    <text id="CharretteKu" order="1179CharretteKu"/>
    <text id="YvainKu" order="1179YvainKu"/>
    <text id="beroul" order="1180beroul"/>
    <text id="eracle" order="1180eracle"/>
    <text id="renart1" order="1180renart1"/>
    <text id="adgar" order="1183adgar"/>
    <text id="BrutCist" order="1183BrutCist"/>
    <text id="DialGreg1" order="1183DialGreg1"/>
    <text id="DialGreg2" order="1183DialGreg2"/>
    <text id="EpMontDeu" order="1183EpMontDeu"/>
    <text id="guingamor" order="1183guingamor"/>
    <text id="orange" order="1183orange"/>
    <text id="PercevalKu" order="1183PercevalKu"/>
    <text id="RecCoulTit" order="1183RecCoulTit"/>
    <text id="RegleSBenCotton" order="1183RegleSBenCotton"/>
    <text id="SBernAn" order="1183SBernAn"/>
    <text id="SermMadn" order="1183SermMadn"/>
    <text id="cobe" order="1185cobe"/>
    <text id="SBernCant" order="1187SBernCant"/>
    <text id="graelent" order="1189graelent"/>
    <text id="BlondNesle" order="1190BlondNesle"/>
    <text id="espine" order="1190espine"/>
    <text id="melion" order="1190melion"/>
    <text id="qlr" order="1190qlr"/>
    <text id="tydorel" order="1190tydorel"/>
    <text id="vidame" order="1190vidame"/>
    <text id="bodelnic" order="1195bodelnic"/>
    <text id="amiamil" order="1200amiamil"/>
    <text id="conttyr_a" order="1200conttyr_a"/>
    <text id="DialAme" order="1200DialAme"/>
    <text id="Elucidaireiii" order="1200Elucidaireiii"/>
    <text id="SAgnesDob" order="1200SAgnesDob"/>
    <text id="escoufle" order="1201escoufle"/>
    <text id="desire" order="1205desire"/>
    <text id="villehardouin1" order="1206villehardouin1"/>
    <text id="villehardouin2" order="1206villehardouin2"/>
    <text id="belinc" order="1207belinc"/>
    <text id="lecheor" order="1209lecheor"/>
    <text id="nabaret" order="1209nabaret"/>
    <text id="clari" order="1210clari"/>
    <text id="dole" order="1210dole"/>
    <text id="trot" order="1210trot"/>
    <text id="aucassin" order="1212aucassin"/>
    <text id="galeran" order="1212galeran"/>
    <text id="tyolet" order="1212tyolet"/>
    <text id="doon" order="1215doon"/>
    <text id="renart10" order="1216renart10"/>
    <text id="renart11" order="1216renart11"/>
    <text id="ombre" order="1220ombre"/>
    <text id="bibleberze" order="1221bibleberze"/>
    <text id="gcoin1" order="1222gcoin1"/>
    <text id="gcoin2" order="1222gcoin2"/>
    <text id="gcoin3" order="1222gcoin3"/>
    <text id="gcoin4" order="1222gcoin4"/>
    <text id="SEustPr1" order="1225SEustPr1"/>
    <text id="tdechamp" order="1226tdechamp"/>
    <text id="qgraal_cm" order="1227qgraal_cm"/>
    <text id="rosel" order="1227rosel"/>
    <text id="violette" order="1228violette"/>
    <text id="fournival" order="1230fournival"/>
    <text id="merlin_suite_litt" order="1232merlin_suite_litt"/>
    <text id="jehan" order="1235jehan"/>
    <text id="SLambert" order="1242SLambert"/>
    <text id="edconfcambr" order="1245edconfcambr"/>
    <text id="ImMondePrK" order="1246ImMondePrK"/>
    <text id="sarrasin" order="1249sarrasin"/>
    <text id="atrper" order="1250atrper"/>
    <text id="bestam" order="1250bestam"/>
    <text id="chartes_aube13" order="1250chartes_aube13"/>
    <text id="chartes_hain13" order="1250chartes_hain13"/>
    <text id="ChirAlbT" order="1250ChirAlbT"/>
    <text id="OrnDamesR" order="1250OrnDamesR"/>
    <text id="raoul" order="1250raoul"/>
    <text id="vergy" order="1250vergy"/>
    <text id="menreims" order="1260menreims"/>
    <text id="MirNDChartr" order="1262MirNDChartr"/>
    <text id="RutebZ" order="1267RutebZ"/>
    <text id="mestiersparis" order="1268mestiersparis"/>
    <text id="trispr" order="1270trispr"/>
    <text id="rosem1" order="1274rosem1"/>
    <text id="rosem2" order="1274rosem2"/>
    <text id="rosem3" order="1274rosem3"/>
    <text id="SBath1" order="1275SBath1"/>
    <text id="SGenPr1" order="1275SGenPr1"/>
    <text id="grchron1" order="1277grchron1"/>
    <text id="roisin" order="1280roisin"/>
    <text id="JAntInv" order="1282JAntInv"/>
    <text id="JAntRect" order="1282JAntRect"/>
    <text id="beauma1" order="1283beauma1"/>
    <text id="SGenPr3" order="1287SGenPr3"/>
    <text id="joinville" order="1307joinville"/>
    <text id="fauvel" order="1310fauvel"/>
    <text id="fouke" order="1317fouke"/>
    <text id="passbonnes" order="1317passbonnes"/>
    <text id="passpal" order="1317passpal"/>
    <text id="plaidsmortemer" order="1320plaidsmortemer"/>
    <text id="moree" order="1322moree"/>
    <text id="sardos" order="1324sardos"/>
    <text id="prunier" order="1325prunier"/>
    <text id="SBath4" order="1335SBath4"/>
    <text id="magloire3" order="1337magloire3"/>
    <text id="grchron9" order="1340grchron9"/>
    <text id="GuillMachFortuneH" order="1341GuillMachFortuneH"/>
    <text id="GuillMachLyonH" order="1342GuillMachLyonH"/>
    <text id="ChaceOisiiT" order="1350ChaceOisiiT"/>
    <text id="GuillMachPlourH" order="1350GuillMachPlourH"/>
    <text id="prov" order="1350prov"/>
    <text id="hlanc" order="1354hlanc"/>
    <text id="daudin" order="1365daudin"/>
    <text id="SGenPr2" order="1367SGenPr2"/>
    <text id="Berin1" order="1370Berin1"/>
    <text id="Berin2" order="1370Berin2"/>
    <text id="oresme" order="1370oresme"/>
    <text id="grchron_j2c5" order="1381grchron_j2c5"/>
    <text id="regcrim1" order="1390regcrim1"/>
    <text id="regcrim2" order="1390regcrim2"/>
    <text id="dictier" order="1392dictier"/>
    <text id="melusine" order="1392melusine"/>
    <text id="menagier" order="1393menagier"/>
    <text id="griseld" order="1395griseld"/>
    <text id="maniere1396" order="1396maniere1396"/>
    <text id="anglure" order="1398anglure"/>
    <text id="maniere1399" order="1399maniere1399"/>
    <text id="dizsages" order="1400dizsages"/>
    <text id="ponthus" order="1400ponthus"/>
    <text id="QJoyesKa" order="1400QJoyesKa"/>
    <text id="froissart1" order="1402froissart1"/>
    <text id="gerson_trinite" order="1402gerson_trinite"/>
    <text id="cdo_peche" order="1404cdo_peche"/>
    <text id="CoutHector1" order="1404CoutHector1"/>
    <text id="pizan_charlesv1" order="1404pizan_charlesv1"/>
    <text id="pizan_charlesv2" order="1404pizan_charlesv2"/>
    <text id="donait" order="1405donait"/>
    <text id="CoutRoum" order="1407CoutRoum"/>
    <text id="baye1" order="1409baye1"/>
    <text id="baye2" order="1409baye2"/>
    <text id="LaurPremCas2_l6" order="1409LaurPremCas2_l6"/>
    <text id="DocSMichel_a" order="1411DocSMichel_a"/>
    <text id="cdo_retenue" order="1414cdo_retenue"/>
    <text id="cdo_ballades" order="1415cdo_ballades"/>
    <text id="maniere1415" order="1415maniere1415"/>
    <text id="SBath3" order="1417SBath3"/>
    <text id="quadrilogue" order="1422quadrilogue"/>
    <text id="parlangl" order="1425parlangl"/>
    <text id="morchesne" order="1426morchesne"/>
    <text id="cdo_complaintes" order="1433cdo_complaintes"/>
    <text id="SGenPr4" order="1433SGenPr4"/>
    <text id="cdo_songe" order="1437cdo_songe"/>
    <text id="ursins" order="1438ursins"/>
    <text id="cdo_rondeaux" order="1443cdo_rondeaux"/>
    <text id="monstre" order="1443monstre"/>
    <text id="VidPierrePetit" order="1444VidPierrePetit"/>
    <text id="louis11_215" order="1450louis11_215"/>
    <text id="SBath2" order="1450SBath2"/>
    <text id="SGenPr5" order="1450SGenPr5"/>
    <text id="coutpoit_a" order="1451coutpoit_a"/>
    <text id="saintre" order="1456saintre"/>
    <text id="artois" order="1460artois"/>
    <text id="jouvencel1" order="1461jouvencel1"/>
    <text id="jouvencel2" order="1461jouvencel2"/>
    <text id="villon" order="1461villon"/>
    <text id="cnn" order="1462cnn"/>
    <text id="pathelin" order="1462pathelin"/>
    <text id="louis11_223" order="1463louis11_223"/>
    <text id="louis11_234" order="1467louis11_234"/>
    <text id="quenouilles1" order="1468quenouilles1"/>
    <text id="louis11_248" order="1470louis11_248"/>
    <text id="quenouilles2" order="1470quenouilles2"/>
    <text id="passauv" order="1477passauv"/>
    <text id="archier" order="1479archier"/>
    <text id="theologie" order="1479theologie"/>
    <text id="tringant" order="1480tringant"/>
    <text id="mirsnicjuif" order="1483mirsnicjuif"/>
    <text id="molinet1" order="1490molinet1"/>
    <text id="jehpar" order="1494jehpar"/>
    <text id="ressource" order="1494ressource"/>
    <text id="phares" order="1496phares"/>
    <text id="commyn1" order="1497commyn1"/>
    <text id="commyn2" order="1497commyn2"/>
    <text id="commyn3" order="1497commyn3"/>
    <text id="commyn4" order="1497commyn4"/>
    <text id="commyn5" order="1497commyn5"/>
    <text id="commyn6" order="1497commyn6"/>
    <text id="commyn7" order="1497commyn7"/>
    <text id="commyn8" order="1497commyn8"/>
    <text id="vigneulles" order="1510vigneulles"/>
  </xsl:variable>
  
</xsl:stylesheet>
