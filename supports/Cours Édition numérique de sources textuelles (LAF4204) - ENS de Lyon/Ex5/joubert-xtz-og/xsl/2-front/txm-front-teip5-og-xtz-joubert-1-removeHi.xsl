<!-- The Identity Transformation -->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:tei="http://www.tei-c.org/ns/1.0">
  <!-- Whenever you match any node or any attribute -->
  <xsl:template match="node()|@*">
    <!-- Copy the current node -->
    <xsl:copy>
      <!-- Including any attributes it has and any child nodes -->
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="tei:hi">
    <xsl:choose>
      <xsl:when test="matches(@rend,'Ancre_de_note_de_bas_de_page|footnote_reference')">
        <xsl:apply-templates/>
      </xsl:when>
      <xsl:when test="not(@rend) and matches(@style,'^(font-size:\d+pt|color:[^;]+)$')">
        <xsl:apply-templates/>
      </xsl:when>
      <xsl:when test="matches(@rend,'[Qq]uote')">
        <quote xmlns="http://www.tei-c.org/ns/1.0">
          <xsl:apply-templates/>
        </quote>
      </xsl:when>
      <xsl:when test="matches(@rend,'[Ll]exmed')">
        <lexmed xmlns="http://www.tei-c.org/ns/1.0">
          <xsl:apply-templates/>
        </lexmed>
      </xsl:when>
      <xsl:when test="matches(@rend,'[Gg]lossaire')">
        <glossaire xmlns="http://www.tei-c.org/ns/1.0">
          <xsl:apply-templates/>
        </glossaire>
      </xsl:when>
      <!-- patch de Æ dans un nom propre -->
      <!--<xsl:when test="matches(.,'^Æ$') and following-sibling::*[1][self::tei:persName]"></xsl:when>-->
      <xsl:otherwise>
        <xsl:copy>
          <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <!-- patch des appel de note mal convertis  -->
  <xsl:template match="tei:hi[matches(@rend,'Ancre_de_note_de_bas_de_page|footnote_reference')]/text()"></xsl:template>
  
  <xsl:template match="tei:seg[preceding-sibling::*[1][self::tei:note] and @style='font-family:Symbol;']"></xsl:template>
  
  <!-- patch de Æ dans un nom propre -->
  <!--<xsl:template match="tei:persName[preceding-sibling::*[1][self::tei:hi and matches(.,'^Æ$')]]">
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <xsl:text>Æ</xsl:text>
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>-->
  
</xsl:stylesheet>
