<!-- The Identity Transformation -->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:tei="http://www.tei-c.org/ns/1.0">
  <!-- Whenever you match any node or any attribute -->
  <xsl:template match="node()|@*">
    <!-- Copy the current node -->
    <xsl:copy>
      <!-- Including any attributes it has and any child nodes -->
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="tei:body/tei:div">
    <xsl:copy>
      <xsl:attribute name="type">livre</xsl:attribute>
      <xsl:attribute name="n">
        <xsl:choose>
          <xsl:when test="matches(tei:head,'^\s*(\d+)\..*$')">
            <xsl:analyze-string select="tei:head" regex="^\s*(\d+)\..*$">
              <xsl:matching-substring><xsl:value-of select="regex-group(1)"/></xsl:matching-substring>
            </xsl:analyze-string>
          </xsl:when>
          <xsl:otherwise>NaN</xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="tei:body/tei:div/tei:div">
    <xsl:copy>
      <xsl:attribute name="type">chapitre</xsl:attribute>
      <xsl:attribute name="n">
        <xsl:choose>
          <xsl:when test="matches(tei:head,'^\s*(\d+)\..*$')">
            <xsl:analyze-string select="tei:head" regex="^\s*(\d+)\..*$">
              <xsl:matching-substring><xsl:value-of select="regex-group(1)"/></xsl:matching-substring>
            </xsl:analyze-string>
          </xsl:when>
          <xsl:otherwise>NaN</xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="tei:div/tei:head">
    <xsl:choose>
      <xsl:when test="matches(.,'^\s*\d+\.\s*\[.*\]\s*$')"></xsl:when>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:apply-templates/>
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="tei:head//text()">
    <xsl:analyze-string select="." regex="^\s*\d+\.">
      <xsl:matching-substring/>
      <xsl:non-matching-substring>
        <xsl:copy-of select="."/>
      </xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:template>
  
</xsl:stylesheet>
