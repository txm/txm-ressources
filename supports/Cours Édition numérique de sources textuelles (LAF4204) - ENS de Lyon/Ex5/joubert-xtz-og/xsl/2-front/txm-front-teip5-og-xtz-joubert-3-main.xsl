<!-- The Identity Transformation -->
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:tei="http://www.tei-c.org/ns/1.0"
  exclude-result-prefixes="#all">
  
  
  <!-- Whenever you match any node or any attribute -->
  <xsl:template match="*|comment()|processing-instruction()|@*">
    <!-- Copy the current node -->
    <xsl:copy>
      <!-- Including any attributes it has and any child nodes -->
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="processing-instruction('xml-model')"></xsl:template>
  <xsl:template match="processing-instruction('xml-stylesheet')"></xsl:template>
  
  <xsl:variable name="filename">
    <xsl:analyze-string select="document-uri(.)" regex="^(.*)/([^/]+)\.[^/]+$">
      <xsl:matching-substring>
        <xsl:value-of select="regex-group(2)"/>
      </xsl:matching-substring>
    </xsl:analyze-string>
  </xsl:variable>
  
  <xsl:variable name="editionName">
    <xsl:choose>
      <xsl:when test="matches($filename,'1579')">Joubert1579</xsl:when>
      <xsl:when test="matches($filename,'1587')">Joubert1587</xsl:when>
      <xsl:otherwise>UNKNOWN</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  
  <xsl:template match="tei:teiHeader">
    <xsl:copy xml:space="preserve">
      <fileDesc xmlns="http://www.tei-c.org/ns/1.0">
        <titleStmt xmlns="http://www.tei-c.org/ns/1.0">
          <title type="main" xmlns="http://www.tei-c.org/ns/1.0">Erreurs populaires (extrait)</title>
          <author>Laurent Joubert</author>
        </titleStmt>
        <editionStmt xmlns="http://www.tei-c.org/ns/1.0">
          <ab xmlns="http://www.tei-c.org/ns/1.0"></ab>
        </editionStmt>
        <publicationStmt xmlns="http://www.tei-c.org/ns/1.0">
          <publisher xmlns="http://www.tei-c.org/ns/1.0">Unpublished</publisher>
          <availability xmlns="http://www.tei-c.org/ns/1.0" status="restricted"><licence>CC-BY-NC-SA</licence></availability>
        </publicationStmt>
        <sourceDesc xmlns="http://www.tei-c.org/ns/1.0"><ab xmlns="http://www.tei-c.org/ns/1.0"></ab></sourceDesc>
      </fileDesc>
      <profileDesc xmlns="http://www.tei-c.org/ns/1.0">
        <creation xmlns="http://www.tei-c.org/ns/1.0"/>
        <langUsage xmlns="http://www.tei-c.org/ns/1.0">
          <language xmlns="http://www.tei-c.org/ns/1.0" ident=""/>
        </langUsage>
      </profileDesc>
    </xsl:copy>
  </xsl:template>
  
  
  <xsl:template match="tei:div">
        <xsl:copy>
          <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    <xsl:text>&#xa;</xsl:text>
  </xsl:template>

<xsl:template match="tei:note">
  <xsl:choose>
    <xsl:when test="matches(.,'^\s*persName.?:')">
      <!-- information tranférée vers un attribut de persName -->
    </xsl:when>
    <xsl:when test="ancestor::tei:sic and matches(.,'^\s*corr.?:')">
<!--      <choice xmlns="http://www.tei-c.org/ns/1.0">
        <sic  xmlns="http://www.tei-c.org/ns/1.0">
          <xsl:value-of select="ancestor::tei:sic"/>
        </sic>
        <corr xmlns="http://www.tei-c.org/ns/1.0">
          <xsl:value-of select="replace(.,'^\s*corr.?:','')"/>
        </corr>
      </choice>-->
    </xsl:when>
    <xsl:when test="matches(.,'^\s*corr.?:') and preceding-sibling::node()[1][self::tei:sic]">
      <xsl:copy>
        <xsl:attribute name="type">corr</xsl:attribute>
        <choice xmlns="http://www.tei-c.org/ns/1.0">
          <sic  xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:call-template name="ex"><xsl:with-param name="string"><xsl:value-of select="preceding-sibling::tei:sic[1]"/></xsl:with-param></xsl:call-template>
          </sic>
          <corr xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:value-of select="replace(.,'^\s*corr.?:|\|','')"/>
          </corr>
        </choice>
      </xsl:copy>
    </xsl:when>
    <xsl:otherwise>
      <xsl:copy>
        <xsl:apply-templates select="@*"/>
        <xsl:choose>
          <xsl:when test="matches(.,'^\s*[Mm]arge.gauche.?:')">
            <xsl:attribute name="type">marge</xsl:attribute>
            <xsl:attribute name="place">margin-left</xsl:attribute>
          </xsl:when>
          <xsl:when test="matches(.,'^\s*[Mm]arge.droite.?:')">
            <xsl:attribute name="type">marge</xsl:attribute>
            <xsl:attribute name="place">margin-right</xsl:attribute>
          </xsl:when>
<!--          <xsl:when test="matches(.,'^\s*[Mm]arge.?:')">
            <xsl:attribute name="type">marge</xsl:attribute>
            <xsl:attribute name="place">margin</xsl:attribute>
          </xsl:when>-->
          <xsl:when test="matches(.,'^\s*transcr.?:')">
            <xsl:attribute name="type">[Tt]ranscr</xsl:attribute>
          </xsl:when>
          <xsl:when test="matches(.,'^\s*[Cc]omment.?:')">
            <xsl:attribute name="type">comment</xsl:attribute>
          </xsl:when>
          <xsl:when test="matches(.,'^\s*[Vv]ariante 1578.?:')">
            <xsl:attribute name="type">variante_1578</xsl:attribute>
          </xsl:when>
          <xsl:when test="matches(.,'^\s*[Gg]lossaire.?:')">
            <xsl:attribute name="type">glossaire</xsl:attribute>
          </xsl:when>
          <xsl:when test="matches(.,'^\s*[Ll]lexmed.?:')">
            <xsl:attribute name="type">lexmed</xsl:attribute>
          </xsl:when>
        </xsl:choose>
        <xsl:apply-templates/>
      </xsl:copy>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- Les paragraphes dans les notes sont supprimés -->

<xsl:template match="tei:note/tei:p">
  <xsl:apply-templates/>
</xsl:template>

  <xsl:template match="tei:p[not(parent::tei:note)]">
    <xsl:choose>
      <xsl:when test="matches(.,'^\s*&lt;\s*([^&gt;|\s]*)\|?([^&gt;\s]*)\s*&gt;\s*$')">
        <xsl:call-template name="pb-cb">
          <xsl:with-param name="string"><xsl:value-of select="normalize-space(.)"/></xsl:with-param>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
        <xsl:text>&#xa;</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    
  </xsl:template>
  
  
<xsl:template match="tei:lb">
  <xsl:choose>
    <xsl:when test="matches(preceding-sibling::node()[1],'\|\|')"></xsl:when>
    <xsl:when test="matches(preceding::text()[not(ancestor::tei:note) and matches(.,'\S')][1],'\|\|')"></xsl:when>
    <xsl:when test="following-sibling::node()[1][self::text()[matches(.,'^\s*&lt;[cp]b')]]"></xsl:when>
    <xsl:when test="following::text()[matches(.,'\S')][1][matches(.,'^\s*&lt;[cp]b')]"></xsl:when>
    <xsl:otherwise><xsl:copy/></xsl:otherwise>
  </xsl:choose>
</xsl:template>

  <xsl:template match="tei:seg">
    <xsl:choose>
      <xsl:when test="@rend='background(none)'">
        <xsl:apply-templates/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="tei:persName">
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <xsl:choose>
        <xsl:when test="descendant::tei:note[matches(.,'^persName.?:')]">
          <xsl:attribute name="ref"><xsl:value-of select="descendant::tei:note[matches(.,'^persName.?:')][1]//tei:ref/@target"/></xsl:attribute>
          <xsl:attribute name="key"><xsl:value-of select="descendant::tei:note[matches(.,'^persName.?:')][1]//tei:ref"/></xsl:attribute>
        </xsl:when>
        <xsl:when test="following-sibling::*[1][self::tei:note[matches(.,'persName.?:')]]">
          <xsl:attribute name="ref"><xsl:value-of select="following-sibling::tei:note[1]//tei:ref/@target"/></xsl:attribute>
          <xsl:attribute name="key"><xsl:value-of select="following-sibling::tei:note[1]//tei:ref"/></xsl:attribute>
        </xsl:when>
      </xsl:choose>
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="tei:sic[ancestor::tei:note]/text()">
    <xsl:analyze-string select="." regex="-?(&lt;pb[^&gt;]*&gt;)?\|\|">
      <xsl:matching-substring/>
      <xsl:non-matching-substring>
        <xsl:value-of select="."/>
      </xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:template>
  
<xsl:template match="tei:sic">
  <xsl:choose>
    <xsl:when test="preceding::node()[1][self::tei:note]">
      <xsl:apply-templates/>
    </xsl:when>
    <xsl:when test="descendant::tei:note[matches(.,'^\s*corr.?:')] or following-sibling::node()[1][self::tei:note[matches(.,'^\s*corr.?:')]]">
      <xsl:variable name="corr">
        <corr xmlns="http://www.tei-c.org/ns/1.0">
          <xsl:choose>
            <xsl:when test="descendant::tei:note[matches(.,'^\s*corr.?:')]">
              <xsl:value-of select="replace(descendant::tei:note[matches(.,'^\s*corr.?:')][1],'^\s*corr.?:','')"/>
            </xsl:when>
            <xsl:otherwise><xsl:value-of select="replace(following-sibling::tei:note[1],'^\s*corr.?:','')"/></xsl:otherwise>
          </xsl:choose>
        </corr>
      </xsl:variable>
      <choice xmlns="http://www.tei-c.org/ns/1.0">
        <xsl:copy>
          <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
        <corr xmlns="http://www.tei-c.org/ns/1.0">
          <xsl:call-template name="pre-tokenize">
            <xsl:with-param name="string"><xsl:value-of select="$corr"/></xsl:with-param>
          </xsl:call-template>
        </corr>
      </choice>
      <xsl:if test="descendant::tei:note[matches(.,'^\s*corr')]">
        <note type="corr" xmlns="http://www.tei-c.org/ns/1.0">
          <choice xmlns="http://www.tei-c.org/ns/1.0">
            <sic  xmlns="http://www.tei-c.org/ns/1.0">
              <xsl:apply-templates select="node()|@*"/>
            </sic>
            <corr xmlns="http://www.tei-c.org/ns/1.0">
              <xsl:call-template name="lb">
                <xsl:with-param name="string"><xsl:value-of select="$corr"/></xsl:with-param>
              </xsl:call-template>
            </corr>
          </choice>
        </note>
      </xsl:if>

    </xsl:when>
    <xsl:otherwise>
      <xsl:copy>
        <xsl:apply-templates select="@*|node()"/>
      </xsl:copy>
      <xsl:comment>sic sans note</xsl:comment>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>




<xsl:template match="text()">
<!--  <xsl:variable name="preceding-tag">
    <xsl:choose>
      <xsl:when test="preceding-sibling::node()[1][self::tei:seg or self::tei:add or self::tei:del]">y</xsl:when>
      <xsl:otherwise>n</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="following-tag">
    <xsl:choose>
      <xsl:when test="following-sibling::node()[1][self::tei:seg or self::tei:add or self::tei:del]">y</xsl:when>
      <xsl:otherwise>n</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>-->
  <xsl:choose>
    <xsl:when test="ancestor::tei:note and not(preceding-sibling::node())">
      <xsl:value-of select="replace(.,'^\s*(corr|transcr|comment|[Vv]ariante 1578|source|marge[^:]*|persName).{0,2}:\s*','')"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:choose>
        <xsl:when test="matches(.,'((&lt;[cp]b[^>]*>)+)\s*$')">
          <!--<xsl:call-template name="detect-tags"><xsl:with-param name="string">
            <xsl:value-of select="replace(.,'((&lt;[cp]b[^>]*>)+)\s*$','&#xa; $1')"/>
          </xsl:with-param></xsl:call-template>-->
          <xsl:call-template name="pre-tokenize"><xsl:with-param name="string">
            <xsl:value-of select="replace(.,'((&lt;[cp]b[^>]*>)+)\s*$','&#xa; $1')"/>
          </xsl:with-param></xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <!--<xsl:call-template name="detect-tags"/>-->
          <xsl:call-template name="pre-tokenize"></xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
      <!--      <xsl:analyze-string select="." regex="^([A-Za-zŒœÀ-ÿ]*)(.*[^A-Za-zŒœÀ-ÿ])([A-Za-zŒœÀ-ÿ]*)$">
        <xsl:matching-substring>
          <xsl:choose>
            <xsl:when test="$preceding-tag='y' and matches(regex-group(1),'\S')">
              <!-\-<xsl:comment>Déplacé dans la balise précédente : <xsl:value-of select="regex-group(1)"/></xsl:comment>-\->
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="regex-group(1)"/>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:call-template name="choice"><xsl:with-param name="string"><xsl:value-of select="regex-group(2)"/></xsl:with-param></xsl:call-template>
          <!-\-<xsl:value-of select="regex-group(2)"/>-\->
          <xsl:choose>
            <xsl:when test="$following-tag='y' and matches(regex-group(3),'\S')">
              <!-\-<xsl:comment>Déplacé dans la balise suivante : <xsl:value-of select="regex-group(3)"/></xsl:comment>-\->
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="regex-group(3)"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:matching-substring>
        <xsl:non-matching-substring>
          <xsl:choose>
            <xsl:when test="matches(.,'^[A-Za-zŒœÀ-ÿ]+$') and ($preceding-tag='y' or $following-tag='y')">
            </xsl:when>
            <xsl:otherwise>
              <xsl:call-template name="pb-cb"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:non-matching-substring>
      </xsl:analyze-string>-->
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>
  

  <!--<xsl:template name="detect-tags">
    <xsl:param name="string"><xsl:value-of select="."/></xsl:param>
    <xsl:analyze-string select="$string" regex="&lt;(surplus|supplied|sic|hi\([^\)]*\))[^:]*:\s*([^>]*)>">
      <xsl:matching-substring>
        <xsl:variable name="tag"><xsl:value-of select="regex-group(1)"/></xsl:variable>
        <xsl:variable name="content"><xsl:value-of select="regex-group(2)"/></xsl:variable>
        <xsl:choose>
          <xsl:when test="matches($tag,'^hi')">
            <hi xmlns="http://www.tei-c.org/ns/1.0" rend="{replace($tag,'hi\(|\)','')}">
              <xsl:call-template name="pre-tokenize"><xsl:with-param name="string"><xsl:value-of select="$content"/></xsl:with-param></xsl:call-template>
            </hi>
          </xsl:when>
          <xsl:otherwise>
            <xsl:element name="{$tag}" namespace="http://www.tei-c.org/ns/1.0">
              <xsl:call-template name="pre-tokenize"><xsl:with-param name="string"><xsl:value-of select="$content"/></xsl:with-param></xsl:call-template>
            </xsl:element>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:matching-substring>
      <xsl:non-matching-substring><xsl:call-template name="pre-tokenize"></xsl:call-template></xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:template>
 --> 


<xsl:template name="pre-tokenize">
  <xsl:param name="string"><xsl:value-of select="."/></xsl:param>
  <xsl:analyze-string select="$string" regex="([A-Za-zŒœÀ-ÿ*#\-]+|['’]?_|\[\[[^\]]+\]\]|\(\([^\)]+\)\)|-?\|\||-?&lt;[pc]b[^>]*>|\{{[^\}}]+\}}\}}|-_)+(['’]\+?|-\+|\+)?|[.,:;?()]">
    <xsl:matching-substring>
      <xsl:choose>
        <xsl:when test="matches(.,'^(&lt;[pc]b[^>]*>)+$')">
          <xsl:call-template name="pb-cb"><xsl:with-param name="string"><xsl:value-of select="."/></xsl:with-param></xsl:call-template>
        </xsl:when>
        <xsl:when test="matches(.,'^(&lt;§[^>]*>)+$')">
          <!-- numéros de paragraphe remontés dans les attributs -->
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>&#xa;</xsl:text>
          <w xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:if test="matches(.,'^[uv]')">*</xsl:if>
            <!--<xsl:comment><xsl:value-of select="."/></xsl:comment>-->
            <xsl:call-template name="ex"><xsl:with-param name="string"><xsl:value-of select="."/></xsl:with-param></xsl:call-template>
          </w>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:matching-substring>
    <xsl:non-matching-substring>
      <xsl:call-template name="ex"><xsl:with-param name="string"><xsl:value-of select="."/></xsl:with-param></xsl:call-template>
    </xsl:non-matching-substring>
  </xsl:analyze-string>
  
</xsl:template>

  
  <xsl:template name="ex">
    <xsl:param name="string"><xsl:value-of select="."/></xsl:param>
    <xsl:analyze-string select="$string" regex="\(\([^\)]+\)\)">
      <xsl:matching-substring>
        <xsl:variable name="substring"><xsl:value-of select="replace(.,'[\(\)]','')"/></xsl:variable>
        <ex xmlns="http://www.tei-c.org/ns/1.0"><xsl:call-template name="supplied"><xsl:with-param name="string"><xsl:value-of select="$substring"/></xsl:with-param></xsl:call-template></ex>
      </xsl:matching-substring>
      <xsl:non-matching-substring>
        <xsl:call-template name="supplied"><xsl:with-param name="string"><xsl:value-of select="."/></xsl:with-param></xsl:call-template>
      </xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:template>
  
  <xsl:template name="supplied">
    <xsl:param name="string"><xsl:value-of select="."/></xsl:param>
    <xsl:analyze-string select="$string" regex="\(\([^\)]+\)\)">
      <xsl:matching-substring>
        <xsl:variable name="substring"><xsl:value-of select="replace(.,'[&lt;+>]','')"/></xsl:variable>
        <supplied xmlns="http://www.tei-c.org/ns/1.0"><xsl:call-template name="lettrine"><xsl:with-param name="string"><xsl:value-of select="$substring"/></xsl:with-param></xsl:call-template></supplied>
      </xsl:matching-substring>
      <xsl:non-matching-substring>
        <xsl:call-template name="lettrine"><xsl:with-param name="string"><xsl:value-of select="."/></xsl:with-param></xsl:call-template>
      </xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:template>
  
  <xsl:template name="lettrine">
    <xsl:param name="string"><xsl:value-of select="."/></xsl:param>
    <xsl:analyze-string select="$string" regex="\{{\{{([^\}}]+)\}}\}}">
      <xsl:matching-substring>
        <xsl:variable name="substring"><xsl:value-of select="replace(.,'[\{{\}}]','')"/></xsl:variable>
        <g xmlns="http://www.tei-c.org/ns/1.0" type="initiale">
          <xsl:analyze-string select="$substring" regex="^([^:]+):([^:]*):([^:]*):([^:]*)$">
            <xsl:matching-substring>
              <xsl:attribute name="rend">
                <xsl:text>size(</xsl:text>
                <xsl:value-of select="regex-group(2)"/>
                <xsl:text>) sizeAct(</xsl:text>
                <xsl:value-of select="regex-group(3)"/>
                <xsl:text>) color(</xsl:text>
                <xsl:value-of select="regex-group(4)"/>
                <xsl:text>)</xsl:text>
              </xsl:attribute>
              <xsl:value-of select="regex-group(1)"/>
            </xsl:matching-substring>
            <xsl:non-matching-substring>
              <xsl:attribute name="rend">
                <xsl:value-of select="replace(.,'^(\*.)(.*)$','$2')"/>
              </xsl:attribute>
              <xsl:value-of select="replace(.,'^(\*.)(.*)$','$1')"/>
            </xsl:non-matching-substring>
          </xsl:analyze-string>
        </g>
      </xsl:matching-substring>
      <xsl:non-matching-substring>
        <xsl:call-template name="pb-cb"><xsl:with-param name="string"><xsl:value-of select="."/></xsl:with-param></xsl:call-template>
      </xsl:non-matching-substring>
    </xsl:analyze-string>
    
  </xsl:template>

<xsl:template name="pb-cb">
  <xsl:param name="string"><xsl:value-of select="."/></xsl:param>
  <xsl:analyze-string select="$string" regex="(-?)&lt;\s*(pb|cb)\s+([^\|]+)\|\s*(\d*)\s*&gt;">
    <xsl:matching-substring>
      <xsl:element name="{regex-group(2)}" namespace="http://www.tei-c.org/ns/1.0">
        <xsl:attribute name="n"><xsl:value-of select="replace(regex-group(3),'\s','')"/></xsl:attribute>
        <xsl:if test="matches(regex-group(1),'-')">
          <xsl:attribute name="rend">hyphen(-)</xsl:attribute>
        </xsl:if>
        
          <xsl:if test="matches(regex-group(4),'^\d+$')">
            <xsl:attribute name="facs">
              <xsl:text>http://perso.ens-lyon.fr/alexei.lavrentev/img/</xsl:text>
              <xsl:value-of select="$editionName"/>
              <xsl:text>/</xsl:text>
              <xsl:value-of select="$editionName"/>
              <xsl:text>_Page_</xsl:text>
              <xsl:choose>
                <xsl:when test="string-length(regex-group(4)) = 1">00</xsl:when>
                <xsl:when test="string-length(regex-group(4)) = 2">0</xsl:when>
              </xsl:choose>
              <xsl:value-of select="regex-group(4)"/>
              <xsl:text>.jpg</xsl:text>
            </xsl:attribute>
          </xsl:if>
        
      </xsl:element>
    </xsl:matching-substring>
    <xsl:non-matching-substring>
      <xsl:call-template name="lb"><xsl:with-param name="string"><xsl:value-of select="."/></xsl:with-param></xsl:call-template>
    </xsl:non-matching-substring>
  </xsl:analyze-string>
</xsl:template>
  
  <xsl:template name="lb">
    <xsl:param name="string"><xsl:value-of select="."/></xsl:param>
    <xsl:analyze-string select="$string" regex="\s*-?\s*\|\|">
      <xsl:matching-substring>
        <xsl:element name="lb" namespace="http://www.tei-c.org/ns/1.0">
          <xsl:attribute name="break">no</xsl:attribute>
          <xsl:if test="matches($string,'-')">
            <xsl:attribute name="rend">hyphen(-)</xsl:attribute>
          </xsl:if>
        </xsl:element>
      </xsl:matching-substring>
      <xsl:non-matching-substring>
        <xsl:value-of select="."/>
      </xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:template>
  
  

  
  
  </xsl:stylesheet>
