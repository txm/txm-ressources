<!-- The Identity Transformation -->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:tei="http://www.tei-c.org/ns/1.0">
  <!-- Whenever you match any node or any attribute -->
  <xsl:template match="node()|@*">
    <!-- Copy the current node -->
    <xsl:copy>
      <!-- Including any attributes it has and any child nodes -->
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="tei:body/tei:div[1]">
    <xsl:if test="tei:p[1]//tei:pb">
      <xsl:copy-of select="tei:p[1]//tei:pb[1]"></xsl:copy-of>
    </xsl:if>
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="tei:body/tei:div/tei:p[1][descendant::tei:pb and not(descendant::tei:w)]"/>
  
  
  
  
</xsl:stylesheet>
