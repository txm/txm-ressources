<?xml version="1.0"?>
<xsl:stylesheet xmlns:edate="http://exslt.org/dates-and-times"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0"
  xmlns:txm="http://textometrie.org/1.0"
  exclude-result-prefixes="tei edate txm" xpath-default-namespace="http://www.tei-c.org/1.0" version="2.0">

  <!--
This software is dual-licensed:

1. Distributed under a Creative Commons Attribution-ShareAlike 3.0
Unported License http://creativecommons.org/licenses/by-sa/3.0/ 

2. http://www.opensource.org/licenses/BSD-2-Clause
		
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

This software is provided by the copyright holders and contributors
"as is" and any express or implied warranties, including, but not
limited to, the implied warranties of merchantability and fitness for
a particular purpose are disclaimed. In no event shall the copyright
holder or contributors be liable for any direct, indirect, incidental,
special, exemplary, or consequential damages (including, but not
limited to, procurement of substitute goods or services; loss of use,
data, or profits; or business interruption) however caused and on any
theory of liability, whether in contract, strict liability, or tort
(including negligence or otherwise) arising in any way out of the use
of this software, even if advised of the possibility of such damage.

     
This stylesheet adds a ref attribute to w elements that will be used for
references in TXM concordances. Can be used with TXM XTZ import module.

Written by Alexei Lavrentiev, UMR 5317 IHRIM, 2017
  -->


  <xsl:output method="xml" encoding="utf-8" omit-xml-declaration="no"/> 
  
  
  <!-- General patterns: all elements, attributes, comments and processing instructions are copied -->
  
  <xsl:template match="*" mode="#all">      
        <xsl:copy>
          <xsl:apply-templates select="*|@*|processing-instruction()|comment()|text()" mode="#current"/>
        </xsl:copy>    
  </xsl:template>
  
  <xsl:template match="*" mode="position"><xsl:value-of select="count(preceding-sibling::*)"/></xsl:template>

  <xsl:template match="@*|comment()|processing-instruction()" mode="#all">
    <xsl:copy/>
  </xsl:template>
  
  <xsl:template match="/tei:TEI">
    <xsl:copy>
      <xsl:namespace name="txm">http://textometrie.org/1.0</xsl:namespace>
      <xsl:apply-templates select="@*"/>
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:variable name="filename">
    <xsl:analyze-string select="document-uri(.)" regex="^(.*)/([^/]+)\.[^/]+$">
      <xsl:matching-substring>
        <xsl:value-of select="regex-group(2)"/>
      </xsl:matching-substring>
    </xsl:analyze-string>
  </xsl:variable>
  
  <xsl:template match="tei:w[not(ancestor::tei:note)]">
    <xsl:variable name="ref">
      <xsl:choose>
        <xsl:when test="ancestor::tei:text/@*:id"><xsl:value-of select="ancestor::tei:text[1]/@*:id[1]"/></xsl:when>
        <xsl:otherwise><xsl:value-of select="$filename"></xsl:value-of></xsl:otherwise>
      </xsl:choose>
        <xsl:text>, </xsl:text>
      <xsl:if test="preceding::tei:pb[1]/@n and not(matches(preceding::tei:cb[1]/@n,'^\d'))">
        <xsl:text>p. </xsl:text>
        <xsl:value-of select="preceding::tei:pb[1]/@n"/>
      </xsl:if>
      <xsl:if test="preceding::tei:pb[1]/@n and preceding::tei:cb[1]/@n and not(matches(preceding::tei:cb[1]/@n,'^\d'))">
        <xsl:text>, </xsl:text>
      </xsl:if>
      <xsl:if test="preceding::tei:cb[1]/@n">
        <xsl:text>col. </xsl:text>
        <xsl:value-of select="preceding::tei:cb[1]/@n"/>
      </xsl:if>
      <xsl:if test="(ancestor::tei:text/@*:id or preceding::tei:pb[1]/@n or preceding::tei:cb[1]/@n) and preceding::tei:lb[1]/@n">
        <xsl:text>, </xsl:text>
      </xsl:if>
      <xsl:if test="preceding::tei:lb[1]/@n">
        <xsl:text>l. </xsl:text>
        <xsl:value-of select="preceding::tei:lb[1]/@n"/>
      </xsl:if>
    </xsl:variable>
        <xsl:copy>
          <xsl:apply-templates select="@*"/>
          <xsl:attribute name="ref"><xsl:value-of select="$ref"/></xsl:attribute>
          <xsl:attribute name="aggl">
            <xsl:choose>
              <xsl:when test="matches(.,'\+')">aggl</xsl:when>
              <xsl:when test="matches(.,'[''’]$')">elision</xsl:when>
              <xsl:otherwise>no</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <xsl:attribute name="dipl"><xsl:apply-templates select="*|text()" mode="dipl"/></xsl:attribute>
          <!--<xsl:apply-templates select="*|processing-instruction()|comment()|text()" mode="norm"/>-->
          <xsl:apply-templates mode="norm"/>
          <!-- provoque txm:form imbriqués -->
          <!--<txm:form xmlns="http://textometrie.org/1.0"><xsl:apply-templates select="*|processing-instruction()|comment()|text()" mode="norm"/></txm:form>
          <txm:ana xmlns="http://textometrie.org/1.0" resp="#txm" type="#dipl"><xsl:apply-templates select="*|processing-instruction()|comment()|text()" mode="dipl"/></txm:ana>-->
        </xsl:copy>
  </xsl:template>  
  
  <!-- Patch erreur de tokenisation -->
  <xsl:template match="tei:w[ancestor::tei:note]">
    <xsl:apply-templates select="descendant::text()"/>
    <xsl:choose>
      <xsl:when test="matches(following::tei:w[1],'^\s*[.,\]\)]')"></xsl:when>
      <!--<xsl:when test="matches(following::tei:w[1],'^\s*-\w+')"></xsl:when>-->
      <xsl:when test="following-sibling::node()[1][self::text()][matches(.,'^\s*-\s*$')]"></xsl:when>
      <xsl:when test="matches(.,'[''’\(\[]\s*$')"></xsl:when>
      <xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <!--<xsl:template match="tei:ex" mode="norm" xml:space="default"><xsl:apply-templates/></xsl:template>-->
  
  <xsl:template match="tei:choice" mode="norm">
    <xsl:apply-templates select="tei:corr|tei:reg|tei:expan" mode="#current"/>
  </xsl:template>
  
  <xsl:template match="tei:choice" mode="dipl">
    <xsl:apply-templates select="tei:sic|tei:orig|tei:abbr" mode="#current"/>
  </xsl:template>
  
  <xsl:template match="tei:lb" mode="dipl">
    <xsl:if test="@rend='hyphen(-)'"><xsl:text>-</xsl:text></xsl:if>
    <xsl:text>||</xsl:text>
  </xsl:template>
  
  <xsl:template match="tei:ex" mode="dipl">‹<xsl:apply-templates mode="#current"/>›</xsl:template>
  
  <xsl:template match="tei:note" mode="#all">
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <xsl:attribute name="fromPage">#<xsl:value-of select="preceding::tei:pb[1]/@xml:id"/></xsl:attribute>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="tei:g[@type='initiale']" mode="dipl">
    <xsl:text>{</xsl:text>
    <!--<xsl:apply-templates mode="#current"/>-->
    <xsl:value-of select="normalize-space(.)"/>
    <xsl:text>}</xsl:text>
  </xsl:template>
  
  <!-- patch pour la création des pages synoptiques -->
  
  <!--<xsl:template match="tei:pb" mode="#all"></xsl:template>-->
  
  
  <xsl:template match="text()" mode="norm">
    <xsl:analyze-string select="normalize-space(.)" regex="#|-?\+">
      <xsl:matching-substring></xsl:matching-substring>
      <xsl:non-matching-substring><xsl:call-template name="asterisque-norm"/></xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:template>
  
  <xsl:template match="text()" mode="dipl">
    <xsl:analyze-string select="normalize-space(.)" regex="['’]?\+$">
      <xsl:matching-substring></xsl:matching-substring>
      <xsl:non-matching-substring>
        <xsl:call-template name="ramiste-dipl"/>
      </xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:template>
  
  <xsl:template match="tei:note//text()">
    <xsl:analyze-string select="." regex="\*(.)">
      <xsl:matching-substring>
        <xsl:choose>
          <xsl:when test="matches(regex-group(1),'[IJUVijuv]')"><xsl:value-of select="regex-group(1)"/></xsl:when>
          <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
        </xsl:choose>
      </xsl:matching-substring>
      <xsl:non-matching-substring>
        <xsl:value-of select="normalize-space(.)"/>
      </xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:template>
  
  <xsl:template name="asterisque-norm">
    <xsl:analyze-string select="." regex="\*(.)">
      <xsl:matching-substring>
        <xsl:choose>
          <xsl:when test="matches(regex-group(1),'[IJUVijuv]')"><xsl:value-of select="regex-group(1)"/></xsl:when>
          <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
        </xsl:choose>
      </xsl:matching-substring>
      <xsl:non-matching-substring>
        <xsl:call-template name="deglutinations-norm"/>
      </xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:template>
  
  <xsl:template name="ramiste-dipl">
    <xsl:param name="string"><xsl:value-of select="."/></xsl:param>
    <xsl:analyze-string select="$string" regex="#?(\*?)([IJUVijuv])">
      <xsl:matching-substring>
        <xsl:variable name="star"><xsl:value-of select="regex-group(1)"/></xsl:variable>
        <xsl:variable name="letter"><xsl:value-of select="regex-group(2)"/></xsl:variable>
        <xsl:call-template name="capitals-dipl">
          <xsl:with-param name="string">
            <xsl:choose>
              <xsl:when test="matches($star,'\*')">
                <xsl:value-of select="translate($letter,'IJUVijuv','JJUUjjvv')"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="translate($letter,'IJUVijuv','IIVViiuu')"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:matching-substring>
      <xsl:non-matching-substring>
        <xsl:call-template name="capitals-dipl"/>
      </xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:template>

<xsl:template name="capitals-dipl">
  <xsl:param name="string"><xsl:value-of select="."/></xsl:param>
  <xsl:analyze-string select="$string" regex="#([A-Za-zéçïü])">
    <xsl:matching-substring>
      <xsl:choose>
        <xsl:when test="matches(regex-group(1),'[A-Z]')">
          <xsl:value-of select="lower-case(regex-group(1))"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="upper-case(regex-group(1))"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:matching-substring>
    <xsl:non-matching-substring>
      <xsl:call-template name="deglutinations-dipl"/>
    </xsl:non-matching-substring>
  </xsl:analyze-string>
</xsl:template>
  
  
  
  <!--<xsl:template name="ponct-norm">
    <xsl:param name="string"><xsl:value-of select="."/></xsl:param>
    <xsl:analyze-string select="$string" regex="^((.*)%.*%.*|([.,]))$">
      <xsl:matching-substring>
        <xsl:choose>
          <xsl:when test="matches(regex-group(2),'\S')">
            <xsl:value-of select="regex-group(2)"></xsl:value-of>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="regex-group(3)"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:matching-substring>
      <xsl:non-matching-substring>
        <xsl:call-template name="deglutinations"></xsl:call-template>
      </xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:template>
  
  <xsl:template name="ponct-dipl">
    <xsl:param name="string"><xsl:value-of select="."/></xsl:param>
    <xsl:analyze-string select="$string" regex="^(.*%(.*)%.*|([.,]))$">
      <xsl:matching-substring>
        <xsl:choose>
          <xsl:when test="matches(regex-group(2),'\S')">
            <xsl:value-of select="regex-group(2)"/>
          </xsl:when>
          <xsl:when test="matches(regex-group(3),'\S')">
            <!-\- ponctuation diplomatique "par défaut" -\->
            <xsl:text>.</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <!-\- pas de ponctuation diplomatique-\->
          </xsl:otherwise>
        </xsl:choose>
      </xsl:matching-substring>
      <xsl:non-matching-substring>
        <xsl:call-template name="deglutinations"></xsl:call-template>
      </xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:template>-->
  
  <xsl:template name="deglutinations-norm">
    <xsl:param name="string"><xsl:value-of select="."/></xsl:param>
    <xsl:analyze-string select="$string" regex="-?_">
      <xsl:matching-substring>
        <xsl:choose>
          <xsl:when test="matches($string,'-')"/>
          <xsl:otherwise>
            <space xmlns="http://www.tei-c.org/ns/1.0">
              <xsl:attribute name="type">degl</xsl:attribute>
            </space>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:matching-substring>
      <xsl:non-matching-substring>
        <xsl:value-of select="."/>
      </xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:template>
  
  <xsl:template name="deglutinations-dipl">
    <xsl:param name="string"><xsl:value-of select="."/></xsl:param>
    <xsl:analyze-string select="$string" regex="-?_">
      <xsl:matching-substring>
        <xsl:choose>
          <xsl:when test="matches($string,'-')">-</xsl:when>
          <xsl:otherwise>
            <space xmlns="http://www.tei-c.org/ns/1.0">
              <xsl:attribute name="type">degl</xsl:attribute>
            </space>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:matching-substring>
      <xsl:non-matching-substring>
        <xsl:value-of select="."/>
      </xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:template>
  

</xsl:stylesheet>