<?xml version="1.0"?>
<xsl:stylesheet xmlns:edate="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:tei="http://www.tei-c.org/ns/1.0" 
	xmlns:txm="http://textometrie.org/1.0"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
                exclude-result-prefixes="#all" version="2.0">
                
	<xsl:output method="xml" encoding="UTF-8" omit-xml-declaration="no" indent="no"/>
	
	<!-- <xsl:output method="xml" encoding="UTF-8" omit-xml-declaration="no" indent="no"  doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/> -->
	
                
                <xsl:strip-space elements="*"/>
                
	<xsl:param name="pagination-element">pb</xsl:param>
	
	<xsl:variable name="word-element">
		<xsl:choose>
			<xsl:when test="//tei:c//txm:form">c</xsl:when>
			<xsl:otherwise>w</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="page-number-adjust" as="xs:integer">
		<xsl:choose>
			<xsl:when test="//tei:c//txm:form">1</xsl:when>
			<xsl:otherwise>2</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	

                <xsl:variable name="inputtype">
                	<xsl:choose>
                		<xsl:when test="//tei:w//txm:form">xmltxm</xsl:when>
                		<xsl:otherwise>xmlw</xsl:otherwise>
                	</xsl:choose>
                </xsl:variable>
	
	<xsl:variable name="filename">
		<xsl:analyze-string select="document-uri(.)" regex="^(.*)/([^/]+)\.[^/]+$">
			<xsl:matching-substring>
				<xsl:value-of select="regex-group(2)"/>
			</xsl:matching-substring>
		</xsl:analyze-string>
	</xsl:variable>
                
                <xsl:template match="/">
                	<html>
                		<head>
                			<title><xsl:choose>
                				<xsl:when test="//tei:text/@id"><xsl:value-of select="//tei:text[1]/@id"/></xsl:when>
                				<xsl:otherwise><xsl:value-of select="$filename"/></xsl:otherwise>
                			</xsl:choose></title>
                			<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
                			<!--<link rel="stylesheet" media="all" type="text/css" href="css/bvhepistemon2014.css" />-->
<!--                			<title>
                				<xsl:if test="$author[not(contains(.,'anonym'))]">
                					<xsl:value-of select="$author"/><xsl:text> : </xsl:text>
                				</xsl:if>
                				<xsl:value-of select="$title-normal"/>
                			</title>                                                                -->
                		</head>
                			<xsl:apply-templates select="descendant::tei:text"/>
                	</html>
                </xsl:template>

<xsl:template match="tei:text">
	<body>
		<xsl:if test="$word-element='w'">
			<a class="txm-page" title="1"  next-word-id="w_0"/>
			<div class="metadata-page">
				<h1><xsl:value-of select="@id"></xsl:value-of></h1>
				<h2>Transcription normalisée</h2>
				<br/>
				<table>
					<xsl:for-each select="@*">
						<tr>
							<td><xsl:value-of select="name()"/></td>
							<td><xsl:value-of select="."/></td>
						</tr>
					</xsl:for-each>
				</table>
			</div>
			
		</xsl:if>
		<xsl:apply-templates/>

		<xsl:variable name="previousPageId"><xsl:text>pb_</xsl:text><xsl:value-of select="count(descendant::*[local-name()=$pagination-element])"/></xsl:variable>
		
		<p>
			<xsl:call-template name="footnotes"><xsl:with-param name="previousPageId"><xsl:value-of select="$previousPageId"/></xsl:with-param></xsl:call-template>
		</p>

	</body>
</xsl:template>

                <xsl:template match="*">
                                <xsl:choose>
                                	<xsl:when test="descendant::tei:p|descendant::tei:ab">
                                		<div>
                                			<xsl:call-template name="addClass"/>
                                			<xsl:apply-templates/></div>
                                		<xsl:text>&#xa;</xsl:text>
                                	</xsl:when>
                                	<xsl:otherwise><span>
                                		<xsl:call-template name="addClass"/>
                                		<xsl:if test="self::tei:add[@del]">
                                			<xsl:attribute name="title"><xsl:value-of select="@del"/></xsl:attribute>
                                		</xsl:if>
                                		<xsl:apply-templates/></span>
                                	<xsl:call-template name="spacing"/>
                                	</xsl:otherwise>
                                </xsl:choose>
                </xsl:template>
                
                <xsl:template match="@*|processing-instruction()|comment()">
                                <!--<xsl:copy/>-->
                </xsl:template>
                
<!--                <xsl:template match="comment()">
                                <xsl:copy/>
                </xsl:template>
-->                
                <xsl:template match="text()">
                	<!--<xsl:value-of select="replace(.,'\s+',' ')"/>-->
                                <xsl:value-of select="normalize-space(.)"/>
                	<xsl:call-template name="spacing"/>
                </xsl:template>
                
                <xsl:template name="addClass">
                	<xsl:attribute name="class">
                		<xsl:value-of select="local-name(.)"/>
                		<xsl:if test="@type"><xsl:value-of select="concat('-',@type)"/></xsl:if>
                		<xsl:if test="@subtype"><xsl:value-of select="concat('-',@subtype)"/></xsl:if>
                		<xsl:if test="@rend"><xsl:value-of select="concat('-',@rend)"/></xsl:if>
                	</xsl:attribute>                	
                </xsl:template>
                
                <xsl:template match="tei:p|tei:lg">
                	<p>
                		<xsl:call-template name="addClass"/>
                		<xsl:apply-templates/>
                	</p>
                	<xsl:text>&#xa;</xsl:text>
                </xsl:template>
	
	<xsl:template match="tei:ab">
		<span>
			<xsl:call-template name="addClass"/>
			<xsl:if test="@n and not(*[1][self::tei:lb])">
				<span class="ab_number">
					<xsl:text>&lt;§ </xsl:text>
					<xsl:value-of select="@n"/>
					<xsl:text>> </xsl:text>
				</span>
			</xsl:if>
			<xsl:apply-templates/>
		</span>
		<xsl:text>&#xa;</xsl:text>
	</xsl:template>
	
		
	<xsl:template match="tei:head">
		<h2>
			<xsl:call-template name="addClass"/>
			<xsl:apply-templates/>
		</h2>
	</xsl:template>
                
                <xsl:template match="tei:g[@type='initiale']">
                	<span class="lettrine"><xsl:apply-templates/></span>
                </xsl:template>
                
	<xsl:template match="tei:lb[not(ancestor::tei:note)]">
		<xsl:variable name="lbcount">
			<xsl:choose>
				<xsl:when test="ancestor::tei:ab"><xsl:number from="tei:ab" level="any"/></xsl:when>
				<xsl:when test="ancestor::tei:p"><xsl:number from="tei:p" level="any"/></xsl:when>
				<xsl:otherwise>999</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:if test="ancestor::tei:w and not(preceding-sibling::tei:pb)"><span class="hyphen">-</span></xsl:if>
<!--		<xsl:if test="@rend='hyphen(-)'"><span class="hyphen">-</span></xsl:if>
		<xsl:if test="@rend='hyphen(=)'"><span class="hyphen">=</span></xsl:if>-->
		<!--<xsl:if test="@break='no' and not(preceding-sibling::*[1][local-name()=$pagination-element])"><span class="hyphen">-</span></xsl:if>-->
		<xsl:if test="not($lbcount=1) or preceding-sibling::node()[matches(.,'\S')]"><br/><xsl:text>&#xa;</xsl:text></xsl:if>
<!--		<xsl:if test="@n and not(@rend='prose')">
			<xsl:choose>
				<xsl:when test="matches(@n,'^[0-9]*[05]$')">
					<!-\-<a title="{@n}" class="verseline" style="position:relative"> </a>-\->
					<span class="verseline"><span class="verselinenumber"><xsl:value-of select="@n"/></span></span>
				</xsl:when>
				<xsl:when test="matches(@n,'[^0-9]')">
					<!-\-<a title="{@n}" class="verseline" style="position:relative"> </a>-\->
					<span class="verseline"><span class="verselinenumber"><xsl:value-of select="@n"/></span></span>
				</xsl:when>
				<xsl:otherwise>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>-->
		<xsl:if test="matches(@n,'[05]$')"><span class="verseline"><span class="verselinenumber"><xsl:value-of select="@n"/></span></span></xsl:if>
		<!--<xsl:if test="ancestor::tei:quote or following::tei:lb[1][ancestor::tei:quote]"><span class="verseline"><span class="quotemark">“</span></span></xsl:if>-->
		<xsl:if test="parent::tei:ab[@n] and not(preceding-sibling::*)">
			<span class="ab_number">
				<xsl:text>&lt;§ </xsl:text>
				<xsl:value-of select="parent::tei:ab/@n"/>
				<xsl:text>> </xsl:text>
			</span>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="tei:lb[ancestor::tei:note]"></xsl:template>
	
	<!-- Page breaks -->                
	<xsl:template match="*[local-name()=$pagination-element]">
		
		<xsl:variable name="next-word-position" as="xs:integer">
			<xsl:choose>
				<xsl:when test="following::*[local-name()=$word-element]">
					<xsl:value-of select="count(following::*[local-name()=$word-element][1]/preceding::*[local-name()=$word-element]) + 1"/>
				</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="next-pb-position" as="xs:integer">
			<xsl:choose>
				<xsl:when test="following::*[local-name()=$pagination-element]">
					<xsl:value-of select="count(following::*[local-name()=$pagination-element][1]/preceding::*[local-name()=$word-element]) + 1"/>
				</xsl:when>
				<xsl:otherwise>999999999</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="next-word-id">
			<xsl:choose>
				<xsl:when test="$next-pb-position - $next-word-position = 999999999">w_0</xsl:when>
				<xsl:when test="$next-pb-position &gt; $next-word-position"><xsl:value-of select="following::*[local-name()=$word-element][1]/@id"/></xsl:when>
				<xsl:otherwise>w_0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		
		<xsl:variable name="editionpagetype">
			<xsl:choose>
				<xsl:when test="ancestor::tei:ab">editionpageverse</xsl:when>
				<xsl:otherwise>editionpage</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="pagenumber">
			<xsl:choose>
				<xsl:when test="@n"><xsl:value-of select="@n"/></xsl:when>
				<xsl:when test="@facs"><xsl:value-of select="substring-before(@facs,'.')"/></xsl:when>
				<xsl:otherwise>[NN]</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<!--<xsl:variable name="page_id"><xsl:value-of select="count(preceding::*[local-name()=$pagination-element])"/></xsl:variable>-->
		<xsl:variable name="previousPageId"><xsl:value-of select="preceding::*[local-name()=$pagination-element][1]/@*:id[1]"/></xsl:variable>
		
		<!--<xsl:if test="following-sibling::*[1][self::tei:lb[@break='no']]"><span class="hyphen">-</span></xsl:if>-->
		<xsl:if test="ancestor::tei:w"><span class="hyphen">-</span></xsl:if>

<xsl:call-template name="footnotes"><xsl:with-param name="previousPageId"><xsl:value-of select="$previousPageId"/></xsl:with-param></xsl:call-template>

		<xsl:text>&#xa;</xsl:text>
		<br/><xsl:text>&#xa;</xsl:text>
		
			<a class="txm-page" title="{count(preceding::*[local-name()=$pagination-element]) + $page-number-adjust}" next-word-id="{$next-word-id}"/>
		
		<span class="{$editionpagetype}"><xsl:value-of select="$pagenumber"/></span><!--<br/>--><xsl:text>&#xa;</xsl:text>
	</xsl:template>
	
	<!-- Notes -->
	<xsl:template match="tei:note">
		<!--<span style="color:violet"> [<b>Note :</b> <xsl:apply-templates/>] </span>-->
		<xsl:variable name="note_count"><xsl:value-of select="count(preceding::tei:note[matches(@type,'^(.+)$')]) + 1"/></xsl:variable>
		<xsl:variable name="note_content">
			<xsl:choose>
				<xsl:when test="descendant::txm:form">
					<xsl:for-each select="descendant::txm:form">						
						<xsl:value-of select="."/>
						<xsl:if test="not(matches(following::txm:form[1],'^[.,\)]')) and not(matches(.,'^\S+[''’]$|^[‘\(]$'))">
							<xsl:text> </xsl:text>
						</xsl:if>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise><xsl:value-of select="normalize-space(.)"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="@place='margin-left'"><span class="note-margin-left"><xsl:apply-templates/></span></xsl:when>
			<xsl:when test="@place='margin-right'"><span class="note-margin-right"><xsl:apply-templates/></span></xsl:when>
			<xsl:when test="@place='margin'"><span class="note-margin"><xsl:apply-templates/></span></xsl:when>
			<xsl:when test="matches(@type,'corr|comment')">
				<a title="{$note_content}" class="noteref" href="#{@*:id}" name="ref_{@*:id}">[<xsl:number count="tei:note[matches(@type,'corr|comment')]" level="any"/>]</a>
			</xsl:when>
			<xsl:when test="matches(@type,'variant')">
				<a title="{$note_content}" class="noteref" href="#{@*:id}" name="ref_{@*:id}">[<xsl:number count="tei:note[matches(@type,'variant')]" level="any" from="tei:pb" format="a"/>]</a>
			</xsl:when>
			<!--<xsl:otherwise><span class="noteref" title="{$note_content}">[•]</span></xsl:otherwise>-->
		</xsl:choose>
		<xsl:call-template name="spacing"/>                                
	</xsl:template>
	
	<!--<xsl:template match="tei:bibl">
		<span class="noteref" title="{normalize-space(.)}">[•]</span>
	</xsl:template>-->
	
	<xsl:template match="tei:ref">
		<xsl:text> </xsl:text>
		<a href="{@target}" target="ext"><xsl:apply-templates/></a>
		<xsl:call-template name="spacing"/>
		<xsl:if test="ancestor::tei:note and not(following::node()[1][self::text()][matches(.,'^\s*[.,\)]')])">
			<xsl:text> </xsl:text>
		</xsl:if>
	</xsl:template>
	
		<xsl:template match="tei:persName">
		<xsl:choose>
			<xsl:when test="@ref and descendant::tei:pb">
				<a href="{@ref}" title="{@key}" class="persName" target="names"><xsl:value-of select="descendant::text()[following-sibling::tei:pb]"/></a>
				<xsl:apply-templates select="descendant::tei:pb|tei:lb"></xsl:apply-templates>
				<span class="persName"><xsl:value-of select="descendant::text()[preceding-sibling::tei:pb]"/></span>
			</xsl:when>
			<xsl:when test="@ref">
				<a href="{@ref}" title="{@key}" class="persName" target="names"><xsl:apply-templates/></a>
			</xsl:when>
			<xsl:otherwise><span class="persName"><xsl:apply-templates/></span></xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="tei:quote">
		<span class="quote">
			<!--<xsl:text>«&#xa0;</xsl:text>-->
			<xsl:apply-templates/>
			<!--<xsl:text>&#xa0;»</xsl:text>-->
		</span>
		<xsl:call-template name="spacing"/>
	</xsl:template>
	
	
	<xsl:template match="a[@class='txmpageref']">
		<xsl:copy-of select="."/>
	</xsl:template>
	
	<!--<xsl:template match="tei:note[@place='inline']">
		<span class="noteinline">
			<xsl:apply-templates/>
		</span>
	</xsl:template>
   -->             
                <xsl:template match="//tei:w"><span>
                	<xsl:attribute name="class">
                		<xsl:choose>
                			<!--<xsl:when test="matches(txm:ana[@type='#aggl'],'aggl')">w-aggl</xsl:when>-->
                			<xsl:when test="matches(txm:ana[@type='#aggl'],'fgmt')">w-fgmt</xsl:when>
                			<xsl:otherwise>w</xsl:otherwise>
                		</xsl:choose>
                	</xsl:attribute>
                	<xsl:choose>
                		<xsl:when test="descendant::tei:c//txm:form">
                			<xsl:apply-templates select="descendant::tei:c"/>
                		</xsl:when>
                		<xsl:otherwise>
                			<xsl:if test="@id">
                				<xsl:attribute name="id">
                					<xsl:value-of select="@id"/>
                				</xsl:attribute>
                			</xsl:if>
                			<xsl:attribute name="title">
                				<xsl:if test="@id">
                					<xsl:value-of select="@id"></xsl:value-of>
                				</xsl:if>
                				<xsl:if test="ancestor::tei:corr">
                					<xsl:value-of select="concat(' sic : ',@sic)"/>
                				</xsl:if>
                				<xsl:if test="ancestor::tei:reg">
                					<xsl:value-of select="concat(' orig : ',@orig)"/>
                				</xsl:if>
                				<xsl:choose>
                					<xsl:when test="descendant::txm:ana">	
                						<xsl:for-each select="descendant::txm:ana">
                							<xsl:value-of select="concat(' ',substring-after(@type,'#'),' : ',.)"/>
                						</xsl:for-each>
                					</xsl:when>
                					<xsl:otherwise>
                						<xsl:for-each select="@*[not(local-name()='id')]">
                							<xsl:value-of select="concat(' ',name(.),' : ',.)"/>
                						</xsl:for-each>                                				
                					</xsl:otherwise>
                				</xsl:choose>
                				<xsl:if test="@*[matches(name(.),'pos$')]">
                				</xsl:if>                                		
                			</xsl:attribute>
                			<xsl:choose>
                				<xsl:when test="ancestor::tei:corr[parent::tei:choice]">
                					<span class="w">
                						<xsl:attribute name="id">
                							<xsl:value-of select="ancestor::tei:choice/tei:sic//tei:w[1]/@id"/>
                						</xsl:attribute>
                						<xsl:call-template name="word-content"/>
                					</span>
                				</xsl:when>
                				<xsl:otherwise>
                					<xsl:call-template name="word-content"/>
                				</xsl:otherwise>
                			</xsl:choose>                			
                		</xsl:otherwise>
                	</xsl:choose>
                                </span><xsl:call-template name="spacing"/></xsl:template>
                
                
                <xsl:template name="word-content">
                	<xsl:choose>
                		<xsl:when test="descendant::txm:form">
                			<xsl:apply-templates select="txm:form"/>
                		</xsl:when>
                		<xsl:otherwise><xsl:apply-templates/></xsl:otherwise>
                	</xsl:choose>
                </xsl:template>
<!--                <xsl:template match="//txm:form">
                                <xsl:apply-templates/>
                </xsl:template>
-->                
	
	<xsl:template match="tei:ex">
		<xsl:apply-templates/>
	</xsl:template>
	
	<xsl:template match="tei:choice">
		<xsl:choose>
			<xsl:when test="ancestor::tei:note">
				<xsl:apply-templates select="tei:sic"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="tei:corr"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="txm:form">
		<xsl:apply-templates/>
	</xsl:template>
	
	
	<xsl:template name="footnotes">
		<xsl:param name="previousPageId"></xsl:param>
		
		<xsl:if test="//tei:note[matches(@type,'corr|comment|variante')][@fromPage=concat('#',$previousPageId)]">
			<!-- [@fromPage='{concat(''#'',$previousPageId)}'] -->
			<xsl:text>&#xa;</xsl:text>
			<br/>
			<br/>			
			<span class="footnotes">
				<!--				<xsl:for-each select="//tei:note[matches(@type,'corr|comment|variante')]">
					<xsl:value-of select="@type"/>-<xsl:value-of select="@fromPage"/>-<xsl:value-of select="$previousPageId"/><br/>
				</xsl:for-each>
-->				<xsl:if test="//tei:note[matches(@type,'variante')][@fromPage=concat('#',$previousPageId)]">
					<span class="footnote">
						<span class="footnoteref">Var.</span>
						<xsl:for-each select="//tei:note[matches(@type,'variante')][@fromPage=concat('#',$previousPageId)]">
							<span class="footnoteref-variant">
								<a href="#ref_{@*:id}" name="{@*:id}">
									<xsl:number count="tei:note[matches(@type,'variante')]" from="tei:pb[@id=$previousPageId]" format="a" level="any"></xsl:number>
								</a>
								<xsl:text>. </xsl:text>
							</span>
							<xsl:value-of select="."/>
							<xsl:text> </xsl:text>
							<span class="variant-source"><xsl:value-of select="substring-after(@type,'variante_')"/></span>
							<xsl:text>. </xsl:text>
						</xsl:for-each></span>
					<xsl:if test="//tei:note[matches(@type,'corr|comment')][@fromPage=concat('#',$previousPageId)]">
					</xsl:if>
				</xsl:if>
				
				<xsl:if test="//tei:note[matches(@type,'corr|comment')][@fromPage=concat('#',$previousPageId)]">
					<xsl:for-each select="//tei:note[matches(@type,'corr|comment') and @fromPage=concat('#',$previousPageId)]">
						<xsl:variable name="note_count"><xsl:value-of select="count(preceding::tei:note[matches(@type,'corr|comment')]) + 1"/></xsl:variable>
						<!--<p><xsl:value-of select="$note_count"/>. <a href="#noteref_{$note_count}" name="note_{$note_count}">[<xsl:value-of select="preceding::tei:cb[1]/@xml:id"/>, l. <xsl:value-of select="preceding::tei:lb[1]/@n"/>]</a><xsl:text> </xsl:text> <xsl:value-of select="."/></p>-->
						<span class="footnote">
							<span class="footnoteref">
								<a href="#ref_{@*:id}" name="{@*:id}">
									<xsl:value-of select="$note_count"/>
								</a>
							</span>
							<xsl:if test="@type='corr'">
								<span class="variant-source">
									<xsl:text>sic&#xa0;:</xsl:text>
								</span>
								<xsl:text> </xsl:text>
							</xsl:if>
							<xsl:apply-templates mode="#current"/>
						</span>
					</xsl:for-each>
				</xsl:if>
			</span><xsl:text>&#xa;</xsl:text>                                                                
			
		</xsl:if>
		
	</xsl:template>
	

	<xsl:template name="spacing">
		<xsl:choose>
			<xsl:when test="$inputtype='xmltxm'">
				<xsl:call-template name="spacing-xmltxm"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="spacing-xmlw"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="spacing-xmlw">
		<xsl:choose>
			<xsl:when test="ancestor::tei:w"/>
			<xsl:when test="matches(@rend,'elision')">’</xsl:when>
			<xsl:when test="matches(@rend,'fgmt')"></xsl:when>
			<xsl:when test="matches(@aggl,'fgmt')"></xsl:when>
			<xsl:when test="following::tei:w[1][matches(.,'^\s*[.,)\]]+\s*$')]"/>			
			<xsl:when test="matches(.,'^\s*[(\[‘]+$|\w(''|’)\s*$')"></xsl:when>
			<xsl:when test="position()=last() and (ancestor::tei:choice or ancestor::tei:supplied[not(@rend='multi_s')])"></xsl:when>
			<xsl:when test="following::*[1][self::tei:note] and not(ancestor::tei:note)"></xsl:when>
			<xsl:when test="following::tei:w[1][matches(.,'^\s*[:;!?]+\s*$')] and not(ancestor::tei:note)">
				<xsl:text>&#xa0;</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text> </xsl:text>
			</xsl:otherwise>
		</xsl:choose>                
	</xsl:template>

	<xsl:template name="spacing-xmltxm">
		<xsl:choose>
			<xsl:when test="ancestor::tei:w"/>
			<xsl:when test="ancestor::tei:sic|tei:corr"/>
			<xsl:when test="self::tei:w and matches(descendant::txm:ana[@type='#rend'][1],'elision')">’</xsl:when>
			<xsl:when test="self::tei:w and matches(descendant::txm:ana[@type='#rend'][1],'fgmt')"></xsl:when>
			<xsl:when test="self::tei:w and matches(descendant::txm:ana[@type='#aggl'][1],'fgmt')"></xsl:when>
			<xsl:when test="following::tei:w[1][matches(descendant::txm:form[1],'^[.,)\]]+$')] and not(ancestor::tei:note)"></xsl:when>			
			<xsl:when test="matches(descendant::txm:form[1],'^[(\[‘]+$|\w(''|’)$')"></xsl:when>
			<xsl:when test="position()=last() and (ancestor::tei:choice or ancestor::tei:supplied[not(@rend='multi_s')])"></xsl:when>
			<xsl:when test="following::*[1][self::tei:note] and not(ancestor::tei:note)"></xsl:when>
			<xsl:when test="following::tei:w[1][matches(descendant::txm:form[1],'^[:;!?]+$')] and not(ancestor::tei:note)">
				<xsl:text>&#xa0;</xsl:text>
			</xsl:when>
			<!-- patch txm:form imbriqué -->
			<xsl:when test="matches(descendant::txm:form[1]/txm:form,'^[(\[‘]+$|\w(''|’)$')"></xsl:when>
			<xsl:when test="parent::tei:quote and position()=last()"></xsl:when>
			<xsl:when test="self::text() and matches(.,'[\(''’]\s*$')"></xsl:when>
			<xsl:when test="following-sibling::node()[1][self::text() and matches(.,'^\s*[.,\)\]]')]"></xsl:when>
			<xsl:when test="self::tei:w and following-sibling::node()[1][self::text()[matches(.,'^-$')]]"></xsl:when>
			<xsl:otherwise>
				<xsl:text> </xsl:text>
			</xsl:otherwise>
		</xsl:choose>                
	</xsl:template>

                
</xsl:stylesheet>
