

# Fichiers supports de l’atelier de formation « Import de corpus dans TXM 0.8 » d’Alexey Lavrentev, mars 2020, dans le cadre du cours Édition numérique de sources textuelles (LAF4204) de l’ENS de Lyon, voir les [screencasts](https://txm.gitpages.huma-num.fr/textometrie/Documentation#tutoriels-vidéo)

<center>

### Copyright © Alexey Lavrentev  
#### Mis à disposition sous licence [CC BY](https://creativecommons.org/licenses/by/4.0/deed.fr)

</center>

## Sommaire

* **CoursEdition_ENSL_09_TXM2Import.pdf** : diapositives de l'introduction "Import TXM 1. Introduction"
* **Ex5-instructions.pdf** : séquence des exercices de l'atelier
* répertoires de ressources :
  * **presse-papier** : pour l'exercice 2 Import « Presse-papier »
  * **joubert-txt** : pour l'exercice 3 Import « TXT + CSV »
  * **joubert-xtz-tei** : pour l'exercice 4 Import « XML TEI Zero + CSV » de fichiers XML-TEI
  * **joubert-xtz-og** : pour l'exercice 5 Import « XML TEI Zero + CSV » de fichiers XML-TEI produits par TEIGarage
* fichiers « *.txm » : les corpus binaires résultat de chaque exercice d'importation de corpus

Vous pouvez télécharger tous les fichiers d'un coup à l'aide du bouton de téléchargement situé en haut à droite ![bouton de téléchargement](download-button.png){width=95px}.

## Contenu

<pre>
Ex5
├── joubert-txt
│   ├── joubert1579_1-02.txt
│   ├── joubert1587_1-02.txt
│   ├── metadata.csv
│   └── metadata.xlsx
├── JOUBERT-TXT.txm
├── joubert-xtz-og
│   ├── css
│   │   ├── Joubert.css
│   │   └── txm.css
│   ├── import.xml
│   ├── joubert1579_1-02.xml
│   ├── joubert1587_1-02.xml
│   ├── metadata.csv
│   ├── metadata.xlsx
│   └── xsl
│       ├── 2-front
│       │   ├── 01-txm-front-teip5-og-xtz-joubert-removeAncor.xsl
│       │   ├── 02-txm-front-teip5-og-xtz-joubert-removeIndex.xsl
│       │   ├── 02-txm-front-teip5-og-xtz-joubert-removePb.xsl
│       │   ├── merge-adjacent-elements-pass2.xsl
│       │   ├── merge-adjacent-elements.xsl
│       │   ├── normalize-apostrophe.xsl
│       │   ├── txm-front-teip5-og-xtz-joubert-1-removeHi.xsl
│       │   ├── txm-front-teip5-og-xtz-joubert-2-main.xsl
│       │   ├── txm-front-teip5-og-xtz-joubert-2-processHead.xsl
│       │   ├── txm-front-teip5-og-xtz-joubert-3-addPbId.xsl
│       │   ├── txm-front-teip5-og-xtz-joubert-3-main.xsl
│       │   ├── txm-front-teip5-og-xtz-joubert-4-raisePb.xsl
│       │   └── txm-front-teip5-og-xtz-joubert-5-addPbId.xsl
│       ├── 3-posttok
│       │   └── txm-posttok-joubert.xsl
│       └── 4-edition
│           ├── 1-default-html.xsl
│           ├── 2-default-pager.xsl
│           ├── 3-dipl-html.xsl
│           └── 4-dipl-pager.xsl
├── JOUBERT-XTZ-OG.txm
├── joubert-xtz-tei
│   ├── joubert1579_1-02.xml
│   ├── joubert1587_1-02.xml
│   ├── metadata.csv
│   └── metadata.xlsx
├── JOUBERT-XTZ-TEI.txm
├── presse-papier
│   └── Joubert1579_1-02.docx
└── PRESSE-PAPIER1.txm
</pre>
