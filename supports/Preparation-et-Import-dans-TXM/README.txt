Fichiers supports de la formation "préparation de corpus et import dans TXM"
                            Serge Heiden
                               2015
Liste des documents :
- Support - Atelier preparation de corpus et import dans TXM : support de l'atelier
- Diapositives - Atelier preparation de corpus et import dans TXM : diapositives introductives
- JeuEtiquettesModeleFrancaisTreeTagger : documentation du jeu d'étiquettes
- regex-cheatsheet : documentation de référence des expressions régulières pour le chercher/remplacer
- corpus : extrait des sources du corpus VOEUX au format ODT, TXT et XML
- macros : exemples de macros de traitement de sources
- scripts : exemples de scripts de traitement de sources
- xsl : exemples de filtres XSL d'import

