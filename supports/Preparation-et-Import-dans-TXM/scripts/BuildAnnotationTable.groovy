// import the packages containing the functions we are going to use
import org.txm.Toolbox
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.txm.functions.concordances.*
import org.txm.functions.concordances.comparators.*
import org.txm.functions.ReferencePattern

// get the BFM corpus
println "Building Annotation Table..."
def corpus = CorpusManager.getCorpusManager().getCorpus("DISCOURS")
println "Corpus: $corpus"
def annotationProp = "lemma"
println "annotation property: $annotationProp"
def queryS = '"je"'
println "query: $queryS"
def csvFile = new File("/home/mdecorde/TEMP/AnnotationTable.tsv")
println "CSV file: $csvFile"

// get some properties
def annot = corpus.getProperty(annotationProp)
println "annot: "+annot
def id = corpus.getProperty("id")
println "id: "+id
def word = corpus.getProperty("word")
println "word: "+word
def text = corpus.getStructuralUnit("text")
println "text: "+text
def text_id = text.getProperty("id")
println "text_id: "+text_id

println "Computing concordance..."
def start = System.currentTimeMillis()

// define the references pattern for each concordance line
def referencePattern = new ReferencePattern()
referencePattern.addProperty(text_id)
referencePattern.addProperty(id)
if (annot != null) referencePattern.addProperty(annot)

// compute the concordance with contexts of 15 words on each side of the keyword
def query = new Query(Query.fixQuery(queryS))
def concordance = new Concordance(corpus, query, word, [word, annot], referencePattern, referencePattern, 5, 5)
concordance.computeConcordance();
println "Conc done "+(System.currentTimeMillis()-start)

def writer = csvFile.newWriter("UTF-8");

println "Writing lines..."
start = System.currentTimeMillis()
writer.write("ref"+"\t"+"CG"+"\t"+"keyword"+"\t"+annot+"\t"+""+"\t"+"CD"+"\t"+"id"+"\n")

// define which occurrence properties will be displayed
concordance.setViewProperties([word])

for (int i = 0 ; i < concordance.getNLines() ; i += 1000) {
	List<Line> lines
	if (i+1000 > concordance.getNLines())
		lines = concordance.getLines(i, concordance.getNLines()-1)
	else
		lines = concordance.getLines(i, i+1000)
		
	// println "lines: from "+i
	for (Line l : lines) {
		/*
		 A : référence (nom de texte)
		 B : contexte gauche (10 mots)
		 C : pivot (cf. la remarque sur "ce que")
		 D : pos du pivot
		 E : colonne vide pour les commentaires
		 F : contexte droit (10 mots)
		 G : identifiant de l'occurrence (pour injecter les corrections après)
		 */
		
		String ntext = l.getViewRef().getValue(text_id)
		String lcontext = l.leftContextToString()
		String pivot = l.keywordToString()
		String rien = ""
		String rcontext = l.rightContextToString()
		String ids = l.getViewRef().getValue(id)
		
		String poss = "";
		if (annot != null) poss = l.getViewRef().getValue(annot)
		
		//println l.getKeywordsViewProperties().get(pos)
		writer.write(ntext+"\t"+lcontext+"\t"+pivot+"\t"+poss+"\t"+rien+"\t"+rcontext+"\t"+ids+"\n")
	}
	writer.flush()
}
writer.close()

println "done "+(System.currentTimeMillis()-start)+" results"
println("Concordance "+query.getQueryString()+" saved in file "+csvFile.getAbsolutePath())