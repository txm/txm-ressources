# Tutoriel de conversion de fichiers CoNLL-U de Universal Dependencies Treebanks vers TIGER-XML pour importation dans TXM

1. Récupérer les scripts depuis https://gitlab.huma-num.fr/txm/txm-ressources/-/tree/master/projects/profiterole/scripts/conll2tiger
    - script Perl : conll2tiger-ud.pl
    - script Sh : conllDir2tiger.sh
    - feuille XSLT : tigerXml-commentOutLongSentences.xsl
    - feuille XSLT : tigerXml-sortBfmByDate.xsl (uniquement si besoin de trier par date les textes de la BFM)
2. Placer les fichiers d'annotation .conllu à convertir dans un dossier, par exemple "CONLL"
3. Copier le script conll2tiger-ud.pl dans le dossier CONLL
4. Copier le script conllDir2tiger.sh dans le dossier parent du dossier CONLL
5. S'assurer que le fichier conllDir2tiger.sh est bien exécutable (si besoin '`chmod +x conllDir2tiger.sh`')
6. Ouvrir un terminal (Ctrl-Alt-T) et accéder au répertoire contenant le script conllDir2tiger.sh
7. Lancer la commande `./conllDir2tiger.sh CONLL`\
  → Les résultats se trouvent dans le dossier CONLL/tiger-xml
8. Appliquer la feuille de style tigerXml-commentOutLongSentences.xsl à tous les fichiers du répertoire tiger-xml
  - Dans TXM, utiliser la macro xml/ExecXSL en indiquant les paramètres suivants :
    - XSLFile : \[...]/tigerXml-commentOutLongSentences.xsl (indiquer le chemin complet)
    - inputDirectory : \[...]/CONLL/tiger-xml
    - outputdirectory : \[...]/CONLL/tiger-xml-cut100\
→ Le résultat se trouve dans le répertoire CONLL/tiger-xml-cut100
9. Pour les textes issus de la BFM, afin de disposer les textes chronologiquement dans le corpus TIGER, il est possible d'appliquer la feuille de style tigerXml-sortBfmByDate.xsl
  - Dans TXM, utiliser la macro xml/ExecXSL en indiquant les paramètres suivants :
    - XSLFile : \[...]/tigerXml-sortBfmByDate.xsl (indiquer le chemin complet)
    - inputDirectory : \[...]/CONLL/tiger-xml-cut100
    - outputdirectory : \[...]/CONLL/tiger-xml-cut100-ordered\
  → Le résultat se trouve dans le répertoire CONLL/tiger-xml-cut100-ordered
  - pour d'autres corpus que la BFM, il faut éditer ou fournir en ligne de commande le paramètre XSL "text-order". Ce dernier est composé d'éléments text munis d'atrributs "id" (nom du fichier d'un texte du corpus) et "order" (clé de tri), par exemple :
	`<text id="strasbBfm" order="0842strasbBfm"/>`
10. **Option A :** Importer le corpus TIGER Search dans TXM
  - Depuis TXM
    - si nécessaire, installer l'extension TIGERSearch
    - utiliser la commande Fichier > Importer > XML-TS
    - sélectionner le répertoire des sources CONLL/tiger-xml-cut100
    - donner un nom au corpus (par défaut le nom est généré à partir du nom du répertoire)
    - décocher éventuellement la case "Langue principale > Annoter le corpus" (les corpus UD sont préannotés)
    - lancer l'import et attendre que le nouveau corpus apparaisse dans l'onglet à gauche
11. **Option B :** Importer les annotations du corpus TIGER Search dans un corpus TXM existant\
(le corpus doit avoir les mêmes mots avec les mêmes identifiants xml:id que les terminaux des annotations TIGER-XML)
  - Depuis TXM
    - si nécessaire, installer l'extension TIGERSearch
    - sélectionner le corpus devant recevoir les annotations
    - utiliser la commande TS > Import TIGERSearch Annotations...
    - sélectionner le répertoire CONLL/tiger-xml-cut100
    - lancer l'import par [Valider]\
→ Les outils de TXM utilisant le moteur TIGERSearch peuvent alors être utilisés (TIGER Index, Ratio, Summary, etc.)
12. Exploiter ces annotations avec TXM
  - Depuis TXM
    - si nécessaire, pour TXM 0.8.1 et versions ultérieures 
      - ouvrir le menu Édition > Préférences > TXM > Avancé > Search Engines
      - cocher la case "Show available search engines"
    - sélectionner le corpus
    - lancer la commande Concordances
     - choisir le moteur TIGER
     - saisir une requête TIGERSearch, par exemple :
     ```java
       #pivot:[pos="VERB"]
     & #clause:[cat="root" & type="VFin"]
     & #clause >L #pivot
     & #clause >D #obj:[cat=("obj"|"ccomp"|"obj:advneg"|"obj\:advmod")]
     & #clause >D #suj:[cat=("nsubj"|"csubj")]
     & #obj >L #objhead:[]
     & #suj >L #sujhead:[]
     & #objhead .* #pivot
     & #pivot .* #sujhead
- Pour combiner des requêtes syntaxiques TIGERSearch avec des requêtes sur structures textuelles CQL, par exemple pour chercher une structure syntaxique particulière dans les premiers paragraphes d'un texte ou un sous-ensemble de textes, il suffit de créer un sous-corpus à partir des requêtes sur structures textuelles CQL puis de lui appliquer des commandes utilisant des requêtes syntaxiques TIGERSearch

